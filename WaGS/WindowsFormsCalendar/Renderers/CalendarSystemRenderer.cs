/*
	Copyright 2012 Justin LeCheminant

	This file is part of WindowsFormsCalendar.

	indowsFormsCalendar is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	indowsFormsCalendar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with indowsFormsCalendar.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;
using MaterialSkin;
using MaterialSkin.Animations;

namespace WindowsFormsCalendar
{
	/// <summary>
	/// CalendarRenderer that renders low-intensity calendar for slow computers
	/// </summary>
	public class CalendarSystemRenderer
		: CalendarRenderer
	{
		#region Fields

		private float _selectedItemBorder;

		#endregion Fields

		/// <summary>
		/// Initializes a new instance of the <see cref="CalendarSystemRenderer"/> class.
		/// </summary>
		/// <param name="calendar"></param>
		public CalendarSystemRenderer(Calendar calendar)
			: base(calendar)
		{
			SelectedItemBorder = 1;
		}

		#region Properties

		/// <summary>
		/// Gets or sets the size of the border of selected items
		/// </summary>
		public float SelectedItemBorder
		{
			get { return _selectedItemBorder; }
			set { _selectedItemBorder = value; }
		}

		#endregion Properties

		#region Overrides

		/// <summary>
		/// Paints the background of the calendar
		/// </summary>
		/// <param name="e">Paint info</param>
		public override void OnDrawBackground(CalendarRendererEventArgs e)
		{
			e.Graphics.Clear(e.Calendar.SkinManager.GetApplicationBackgroundColor());
		}

		/// <summary>
		/// Paints the specified day on the calendar
		/// </summary>
		/// <param name="e">Paint info</param>
		public override void OnDrawDay(CalendarRendererDayEventArgs e)
		{
			base.OnDrawDay(e);
		}

		/// <summary>
		/// Paints the border of the specified day
		/// </summary>
		/// <param name="e"></param>
		public override void OnDrawDayBorder(CalendarRendererDayEventArgs e)
		{
			base.OnDrawDayBorder(e);

			var temp = e.Graphics.SmoothingMode;
			e.Graphics.SmoothingMode = SmoothingMode.None;

			Rectangle r = new Rectangle(e.Day.Bounds.Location, new Size(e.Day.Bounds.Width + 1, e.Day.Bounds.Height));
			bool today = e.Day.Date.Date.Equals(DateTime.Today.Date);

			using (Pen p = new Pen(e.Calendar.SkinManager.GetSecondaryTextColor(), 1))
			{
				if (e.Calendar.DaysMode == CalendarDaysMode.Short)
				{
					e.Graphics.DrawLine(p, r.Right, r.Top, r.Right, r.Bottom);
					e.Graphics.DrawLine(p, r.Left, r.Bottom, r.Right, r.Bottom);
				}
				else
				{
					e.Graphics.DrawRectangle(p, r);
				}
			}

			e.Graphics.SmoothingMode = temp;
		}

		/// <summary>
		/// Draws the all day items area
		/// </summary>
		/// <param name="e">Paint Info</param>
		public override void OnDrawDayTop(CalendarRendererDayEventArgs e)
		{
			var temp = e.Graphics.SmoothingMode;
			e.Graphics.SmoothingMode = SmoothingMode.None;

			using (Brush b = new SolidBrush(e.Calendar.SkinManager.GetApplicationBackgroundColor()))
			{
				e.Graphics.FillRectangle(b, e.Day.DayTop.Bounds);
			}

			base.OnDrawDayTop(e);

			e.Graphics.SmoothingMode = temp;
		}

		/// <summary>
		/// Paints the background of the specified day's header
		/// </summary>
		/// <param name="e"></param>
		public override void OnDrawDayHeaderBackground(CalendarRendererDayEventArgs e)
		{
			base.OnDrawDayHeaderBackground(e);
		}

		/// <summary>
		/// Raises the <see cref="E:DrawWeekHeader"/> event.
		/// </summary>
		/// <param name="e">The <see cref="WindowsFormsCalendar.CalendarRendererBoxEventArgs"/> instance containing the event data.</param>
		public override void OnDrawWeekHeader(CalendarRendererBoxEventArgs e)
		{
			e.TextColor = e.Calendar.SkinManager.GetSecondaryTextColor();

			base.OnDrawWeekHeader(e);
		}

		/// <summary>
		/// Draws a time unit of a day
		/// </summary>
		/// <param name="e"></param>
		public override void OnDrawDayTimeUnit(CalendarRendererTimeUnitEventArgs e)
		{
			base.OnDrawDayTimeUnit(e);

			var temp = e.Graphics.SmoothingMode;
			e.Graphics.SmoothingMode = SmoothingMode.None;

			using (SolidBrush b = new SolidBrush(e.Calendar.SkinManager.GetApplicationBackgroundColor()))
			{
				//if (e.Unit.Selected)
				//{
				//    b.Color = e.Calendar.SkinManager.ColorScheme.LightPrimaryColor;
				//}
				//else if (e.Unit.Highlighted)
				//{
				//    b.Color = e.Calendar.SkinManager.GetCheckboxOffColor();
				//}

				e.Graphics.FillRectangle(b, e.Unit.Bounds);
			}

			using (Pen p = new Pen(e.Unit.Minutes == 0 ? e.Calendar.SkinManager.GetSecondaryTextColor() : e.Calendar.SkinManager.GetSecondaryTextColor()))
			{
				e.Graphics.DrawLine(p, e.Unit.Bounds.Location, new Point(e.Unit.Bounds.Right, e.Unit.Bounds.Top));
			}

			e.Graphics.SmoothingMode = temp;
		}

		/// <summary>
		/// Paints the timescale of the calendar
		/// </summary>
		/// <param name="e">Paint info</param>
		public override void OnDrawTimeScale(CalendarRendererEventArgs e)
		{
			var temp = e.Graphics.SmoothingMode;
			e.Graphics.SmoothingMode = SmoothingMode.None;
			int margin = 5;
			int largeX1 = TimeScaleBounds.Left + margin;
			int largeX2 = TimeScaleBounds.Right - margin;
			int shortX1 = TimeScaleBounds.Left + TimeScaleBounds.Width / 2;
			int shortX2 = largeX2;
			int top = 0;
			Pen p = new Pen(e.Calendar.SkinManager.GetSecondaryTextColor());

			for (int i = 0; i < e.Calendar.Days[0].TimeUnits.Length; i++)
			{
				CalendarTimeScaleUnit unit = e.Calendar.Days[0].TimeUnits[i];

				if (!unit.Visible) continue;

				top = unit.Bounds.Top;

				if (unit.Minutes == 0)
				{
					e.Graphics.DrawLine(p, largeX1, top, largeX2, top);
				}
				else
				{
					e.Graphics.DrawLine(p, shortX1, top, shortX2, top);
				}
			}

			if (e.Calendar.DaysMode == CalendarDaysMode.Expanded
				&& e.Calendar.Days != null
				&& e.Calendar.Days.Length > 0
				&& e.Calendar.Days[0].TimeUnits != null
				&& e.Calendar.Days[0].TimeUnits.Length > 0
				)
			{
				top = e.Calendar.Days[0].BodyBounds.Top;

				//Timescale top line is full
				e.Graphics.DrawLine(p, TimeScaleBounds.Left, top, TimeScaleBounds.Right, top);
			}

			p.Dispose();

			e.Graphics.SmoothingMode = temp;
			base.OnDrawTimeScale(e);
		}

		/// <summary>
		/// Paints an hour of a timescale unit
		/// </summary>
		/// <param name="e">Paint Info</param>
		public override void OnDrawTimeScaleHour(CalendarRendererBoxEventArgs e)
		{
			e.TextColor = e.Calendar.SkinManager.GetSecondaryTextColor();
			base.OnDrawTimeScaleHour(e);
		}

		/// <summary>
		/// Paints minutes of a timescale unit
		/// </summary>
		/// <param name="e">Paint Info</param>
		public override void OnDrawTimeScaleMinutes(CalendarRendererBoxEventArgs e)
		{
			e.TextColor = e.Calendar.SkinManager.GetSecondaryTextColor();
			base.OnDrawTimeScaleMinutes(e);
		}

		/// <summary>
		/// Draws the background of the specified item
		/// </summary>
		/// <param name="e">Event Info</param>
		public override void OnDrawItemBackground(CalendarRendererItemBoundsEventArgs e)
		{
			base.OnDrawItemBackground(e);

			int alpha = 255;

			if (e.Item.IsDragging)
			{
				alpha = 120;
			}
			else if (e.Calendar.DaysMode == CalendarDaysMode.Short)
			{
				alpha = 200;
			}

			using (Brush b = new SolidBrush(Color.FromArgb(alpha, e.Item.BackgroundColor)))
			{
				var temp = e.Graphics.SmoothingMode;
				e.Graphics.SmoothingMode = SmoothingMode.None;
				e.Graphics.FillRectangle(b, e.Item.BoxBounds);
				e.Graphics.SmoothingMode = temp;
			}

			if (e.Item.IsAnimating)
			{
				e.Item.Animate(e);
			}
		}

		/// <summary>
		/// Draws the border of the specified item
		/// </summary>
		/// <param name="e">Event Info</param>
		public override void OnDrawItemBorder(CalendarRendererItemBoundsEventArgs e)
		{
			base.OnDrawItemBorder(e);

			Color a = Color.FromArgb(0, e.Item.BackgroundColor);
			Color b = e.Item.Selected && !e.Item.IsDragging ? e.Calendar.SkinManager.GetSecondaryTextColor() : a;
			Color c = Color.FromArgb(e.Item.IsDragging ? 120 : 255, b);

			var temp = e.Graphics.SmoothingMode;
			e.Graphics.SmoothingMode = SmoothingMode.None;
			ItemBorder(e, e.Item.BoxBounds, c, e.Item.Selected && !e.Item.IsDragging ? SelectedItemBorder : 1f);
			e.Graphics.SmoothingMode = temp;
		}

		/// <summary>
		/// Draws the starttime of the item if applicable
		/// </summary>
		/// <param name="e">Event data</param>
		public override void OnDrawItemStartTime(CalendarRendererBoxEventArgs e)
		{
			if (e.TextColor.IsEmpty)
			{
				e.TextColor = e.Calendar.SkinManager.GetSecondaryTextColor();
			}

			base.OnDrawItemStartTime(e);
		}

		/// <summary>
		/// Draws the end time of the item if applicable
		/// </summary>
		/// <param name="e">Event data</param>
		public override void OnDrawItemEndTime(CalendarRendererBoxEventArgs e)
		{
			if (e.TextColor.IsEmpty)
			{
				e.TextColor = e.Calendar.SkinManager.GetSecondaryTextColor();
			}

			base.OnDrawItemEndTime(e);
		}

		/// <summary>
		/// Draws the text of an item
		/// </summary>
		/// <param name="e"></param>
		public override void OnDrawItemText(CalendarRendererBoxEventArgs e)
		{
			CalendarItem item = e.Tag as CalendarItem;

			if (item != null)
			{
				if (item.IsDragging)
				{
					e.TextColor = Color.FromArgb(120, e.TextColor);
				}
			}

			base.OnDrawItemText(e);
		}

		/// <summary>
		/// Raises the <see cref="E:DrawWeekHeaders"/> event.
		/// </summary>
		/// <param name="e">The <see cref="WindowsFormsCalendar.CalendarRendererEventArgs"/> instance containing the event data.</param>
		public override void OnDrawWeekHeaders(CalendarRendererEventArgs e)
		{
			base.OnDrawWeekHeaders(e);
		}

		/// <summary>
		/// Raises the <see cref="E:DrawDayNameHeader"/> event.
		/// </summary>
		/// <param name="e">The <see cref="WindowsFormsCalendar.CalendarRendererBoxEventArgs"/> instance containing the event data.</param>
		public override void OnDrawDayNameHeader(CalendarRendererBoxEventArgs e)
		{
			e.TextColor = e.Calendar.SkinManager.GetSecondaryTextColor();

			base.OnDrawDayNameHeader(e);

			using (Pen p = new Pen(e.Calendar.SkinManager.GetSecondaryTextColor()))
			{
				var temp = e.Graphics.SmoothingMode;
				e.Graphics.SmoothingMode = SmoothingMode.None;
				e.Graphics.DrawLine(p, e.Bounds.Right, e.Bounds.Top, e.Bounds.Right, e.Bounds.Bottom);
				e.Graphics.SmoothingMode = temp;
			}
		}

		/// <summary>
		/// Draws the overflow to end of specified day
		/// </summary>
		/// <param name="e"></param>
		public override void OnDrawDayOverflowEnd(CalendarRendererDayEventArgs e)
		{
			using (GraphicsPath path = new GraphicsPath())
			{
				int top = e.Day.OverflowEndBounds.Top + e.Day.OverflowEndBounds.Height / 2;
				path.AddPolygon(new Point[] {
					new Point(e.Day.OverflowEndBounds.Left, top),
					new Point(e.Day.OverflowEndBounds.Right, top),
					new Point(e.Day.OverflowEndBounds.Left + e.Day.OverflowEndBounds.Width / 2, e.Day.OverflowEndBounds.Bottom),
				});
			}
		}

		#endregion Overrides
	}
}