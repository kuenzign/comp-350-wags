/*
	Copyright 2012 Justin LeCheminant

	This file is part of WindowsFormsCalendar.

	indowsFormsCalendar is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	indowsFormsCalendar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with indowsFormsCalendar.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using MaterialSkin;

namespace WindowsFormsCalendar
{
	/// <summary>
	/// Renders the professional calendar control
	/// </summary>
	public class CalendarProfessionalRenderer
		: CalendarSystemRenderer
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CalendarProfessionalRenderer"/> class.
		/// </summary>
		/// <param name="c">The c.</param>
		public CalendarProfessionalRenderer(Calendar c)
			: base(c)
		{
			SelectedItemBorder = 1f;
			ItemRoundness = 5;
		}

		#region Private Method

		/// <summary>
		/// Gradients the rect.
		/// </summary>
		/// <param name="g">The g.</param>
		/// <param name="bounds">The bounds.</param>
		/// <param name="a">A.</param>
		/// <param name="b">The b.</param>
		public static void GradientRect(Graphics g, Rectangle bounds, Color a, Color b)
		{
			using (LinearGradientBrush br = new LinearGradientBrush(bounds, b, a, -90))
			{
				g.FillRectangle(br, bounds);
			}
		}

		/// <summary>
		/// Glossies the rect.
		/// </summary>
		/// <param name="g">The g.</param>
		/// <param name="bounds">The bounds.</param>
		/// <param name="a">A.</param>
		/// <param name="b">The b.</param>
		/// <param name="c">The c.</param>
		/// <param name="d">The d.</param>
		public static void GlossyRect(Graphics g, Rectangle bounds, Color a, Color b, Color c, Color d)
		{
			Rectangle top = new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height / 2);
			Rectangle bot = Rectangle.FromLTRB(bounds.Left, top.Bottom, bounds.Right, bounds.Bottom);

			GradientRect(g, top, a, b);
			GradientRect(g, bot, c, d);
		}

		#endregion Private Method

		#region Overrides

		/// <summary>
		/// Initializes the Calendar
		/// </summary>
		/// <param name="e"></param>
		public override void OnInitialize(CalendarRendererEventArgs e)
		{
			base.OnInitialize(e);
		}

		/// <summary>
		/// Raises the <see cref="E:DrawDayHeaderBackground"/> event.
		/// </summary>
		/// <param name="e">The <see cref="WindowsFormsCalendar.CalendarRendererDayEventArgs"/> instance containing the event data.</param>
		public override void OnDrawDayHeaderBackground(CalendarRendererDayEventArgs e)
		{
			Rectangle r = e.Day.HeaderBounds;

			var temp = e.Graphics.SmoothingMode;
			e.Graphics.SmoothingMode = SmoothingMode.None;
			e.Graphics.FillRectangle(e.Calendar.SkinManager.ColorScheme.PrimaryBrush, r);

			if (e.Calendar.DaysMode == CalendarDaysMode.Short)
			{
				using (Pen p = new Pen(e.Calendar.SkinManager.GetSecondaryTextColor()))
				{
					e.Graphics.DrawLine(p, r.Left, r.Top, r.Right, r.Top);
					e.Graphics.DrawLine(p, r.Left, r.Bottom, r.Right, r.Bottom);
				}
			}

			e.Graphics.SmoothingMode = temp;
		}

		/// <summary>
		/// Raises the <see cref="E:DrawItemBorder"/> event.
		/// </summary>
		/// <param name="e">The <see cref="WindowsFormsCalendar.CalendarRendererItemBoundsEventArgs"/> instance containing the event data.</param>
		public override void OnDrawItemBorder(CalendarRendererItemBoundsEventArgs e)
		{
			base.OnDrawItemBorder(e);

			if (e.Item.Selected && !e.Item.IsDragging)
			{
				bool horizontal = false;
				bool vertical = false;
				Rectangle r1 = new Rectangle(0, 0, 5, 5);
				Rectangle r2 = new Rectangle(0, 0, 5, 5);

				horizontal = e.Item.IsOnDayTop;
				vertical = !e.Item.IsOnDayTop && e.Calendar.DaysMode == CalendarDaysMode.Expanded;

				if (horizontal)
				{
					r1.X = e.Item.BoxBounds.Left - 2;
					r2.X = e.Item.BoxBounds.Right - r1.Width + 2;
					r1.Y = e.Item.BoxBounds.Top + (e.Item.BoxBounds.Height - r1.Height) / 2;
					r2.Y = r1.Y;
				}

				if (vertical)
				{
					r1.Y = e.Item.BoxBounds.Top - 2;
					r2.Y = e.Item.BoxBounds.Bottom - r1.Height + 2;
					r1.X = e.Item.BoxBounds.Left + (e.Item.BoxBounds.Width - r1.Width) / 2;
					r2.X = r1.X;
				}

				if ((horizontal || vertical) && Calendar.AllowItemResize)
				{
					if (!e.Item.IsOpenStart && e.IsFirst)
					{
						e.Graphics.FillRectangle(Brushes.White, r1);
						e.Graphics.DrawRectangle(Pens.Black, r1);
					}

					if (!e.Item.IsOpenEnd && e.IsLast)
					{
						e.Graphics.FillRectangle(Brushes.White, r2);
						e.Graphics.DrawRectangle(Pens.Black, r2);
					}
				}
			}
		}

		#endregion Overrides
	}
}