﻿#region copyright

////////////////////////////////////////////////
// FileName:          Program.cs
// Namespace:         WaGS.UI
// Project:           WaGS
//
// Created On:        3/31/2017 at 1:40 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using System;
using System.Windows.Forms;

namespace WaGS.UI
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}