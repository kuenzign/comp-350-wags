﻿#region copyright

////////////////////////////////////////////////
// FileName:          AssemblyInfo.cs
// Namespace:         %Namespace%
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo("UnitTesting")]

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WaGS")]
[assembly: AssemblyDescription("What a Great Scheduler")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("6 Puppers LLC")]
[assembly: AssemblyProduct("What a Great Scheduler")]
[assembly: AssemblyCopyright("© 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("03e569f8-7d74-4f01-9db3-7031d436b3fd")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]