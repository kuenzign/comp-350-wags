﻿#region copyright

////////////////////////////////////////////////
// FileName:          HamburgerMenu.cs
// Namespace:         WaGS.UI
// Project:           WaGS
//
// Created On:        5/2/2017 at 7:11 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using MaterialSkin;
using MaterialSkin.Controls;
using PopupControl;
using Svg;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml;

namespace WaGS.UI
{
	public partial class HamburgerMenu : UserControl, IMaterialControl
	{
		[Browsable(false)]
		public int Depth { get; set; }

		[Browsable(false)]
		public MaterialSkinManager SkinManager => MaterialSkinManager.Instance;

		[Browsable(false)]
		public MouseState MouseState { get; set; }

		protected MainForm _mainForm;
		protected MaterialMenuButton _button;

		internal XmlDocument _openImage;
		internal XmlDocument _saveImage;
		internal XmlDocument _listImage;
		internal XmlDocument _themeImage;
		internal XmlDocument _aboutImage;
		internal XmlDocument _exitImage;

		public HamburgerMenu(MainForm form, MaterialMenuButton button)
		{
			_mainForm = form;
			_button = button;
			_openImage = new XmlDocument();
			_openImage.LoadXml(Properties.Resources.open);
			_saveImage = new XmlDocument();
			_saveImage.LoadXml(Properties.Resources.save);
			_listImage = new XmlDocument();
			_listImage.LoadXml(Properties.Resources.list);
			_themeImage = new XmlDocument();
			_themeImage.LoadXml(Properties.Resources.theme);
			_aboutImage = new XmlDocument();
			_aboutImage.LoadXml(Properties.Resources.about);
			_exitImage = new XmlDocument();
			_exitImage.LoadXml(Properties.Resources.exit);
			InitializeComponent();
		}

		private void HamburgerMenu_Paint(object sender, PaintEventArgs e)
		{
			BackColor = SkinManager.GetApplicationBackgroundColor();
			string color = "#" + ColorExtension.ToColor((int)Primary.Grey600).R.ToString("X2") + ColorExtension.ToColor((int)Primary.Grey600).G.ToString("X2") + ColorExtension.ToColor((int)Primary.Grey600).B.ToString("X2");
			_openImage.DocumentElement.Attributes["fill"].Value = color;
			OpenButton.Icon = SvgDocument.Open(_openImage).Draw(24, 24);
			_saveImage.DocumentElement.Attributes["fill"].Value = color;
			SaveButton.Icon = SvgDocument.Open(_saveImage).Draw(24, 24);
			_listImage.DocumentElement.Attributes["fill"].Value = color;
			ShowCourseCodesButton.Icon = SvgDocument.Open(_listImage).Draw(24, 24);
			_themeImage.DocumentElement.Attributes["fill"].Value = color;
			ThemeButton.Icon = SvgDocument.Open(_themeImage).Draw(24, 24);
			_aboutImage.DocumentElement.Attributes["fill"].Value = color;
			AboutButton.Icon = SvgDocument.Open(_aboutImage).Draw(24, 24);
			_exitImage.DocumentElement.Attributes["fill"].Value = color;
			ExitButton.Icon = SvgDocument.Open(_exitImage).Draw(24, 24);
			using(var pen = new Pen(SkinManager.GetDividersColor(), 1)){
				var temp = e.Graphics.SmoothingMode;
				e.Graphics.SmoothingMode = SmoothingMode.None;
				e.Graphics.DrawLine(pen, Width - 1, 0, Width - 1, Height);
				e.Graphics.SmoothingMode = temp;
			}
		}

		private void OpenButton_Click(object sender, EventArgs e)
		{
			_button.CloseMenu();
			((Popup) Parent).Close();
			_mainForm.ClickOpen(sender, e);
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			_button.CloseMenu();
			((Popup) Parent).Close();
			_mainForm.ClickSave(sender, e);
		}

		private void ShowCourseCodesButton_Click(object sender, EventArgs e)
		{
			_button.CloseMenu();
			((Popup) Parent).Close();
			_mainForm.ClickShowCourseCodes(sender, e);
		}

		private void ThemeButton_Click(object sender, EventArgs e)
		{
			_button.CloseMenu();
			((Popup) Parent).Close();
			_mainForm.ClickTheme(sender, e);
			Invalidate(true);
		}

		private void AboutButton_Click(object sender, EventArgs e)
		{
			_button.CloseMenu();
			((Popup) Parent).Close();
			_mainForm.ClickAbout(sender, e);
		}

		private void ExitButton_Click(object sender, EventArgs e)
		{
			_button.CloseMenu();
			((Popup) Parent).Close();
			_mainForm.Close();
		}
	}
}