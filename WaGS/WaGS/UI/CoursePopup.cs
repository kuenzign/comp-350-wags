﻿#region copyright

////////////////////////////////////////////////
// FileName:          CoursePopup.cs
// Namespace:         WaGS.UI
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using MaterialSkin;
using MaterialSkin.Controls;
using PopupControl;
using Svg;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using WaGS.Core;

namespace WaGS.UI
{
	internal partial class CoursePopup : UserControl, IMaterialControl
	{
		[Browsable(false)]
		public int Depth { get; set; }

		[Browsable(false)]
		public MaterialSkinManager SkinManager => MaterialSkinManager.Instance;

		[Browsable(false)]
		public MouseState MouseState { get; set; }

		protected Section _section;
		protected MainForm _mainForm;
		internal XmlDocument _deleteImage;

		public CoursePopup(MainForm form, Section section, bool hasRemoveButton, CourseList completed = null)
		{
			InitializeComponent();
			ResizeRedraw = true;
			_deleteImage = new XmlDocument();
			_deleteImage.LoadXml(Properties.Resources.delete);
			MinimumSize = Size;
			MaximumSize = new Size(Size.Width * 2, Size.Height * 2);
			DoubleBuffered = true;
			ResizeRedraw = true;
			nameLabel.Text = section.Parent.Department + " " + section.Parent.CourseNumber.ToString() + " " + section.SectionLetter + ": " + section.Parent.ShortTitle;
            if (completed != null)
            {
                bool foundPre = false;
                foreach (Course pre in section.Parent.PrerequisiteList)
                {
                    if (!completed.Contains(pre)) {
                        prerequisiteLabel.Text += pre.Department + " " + pre.CourseNumber;
                        foundPre = true;
                    }
                }
                if(foundPre)
                    prerequisiteLabel.Text = "Missing Prerequisites " + prerequisiteLabel.Text;
                prerequisiteLabel.Visible = foundPre;
            }

			if(section.Times.Count > 0)
			{
				string Days = "";
				foreach (TimeSlot ts in section.Times)
				{
					Days += ts.dayOfWeek;
				}

				locationLabel.Text = section.Times[0].Locale.Building + " " + section.Times[0].Locale.RoomNumber;
				timeLabel.Text = Days + " " + section.Times[0].StartTime.ToShortTimeString() + " - " + section.Times[0].EndTime.ToShortTimeString();
			}
			else
			{
				locationLabel.Text = "No Room";
				timeLabel.Text = "No Time";
			}
			capacityLabel.Text = section.Enrollment+"/"+section.Capacity+" Seats filled";
			removeButton.Visible = hasRemoveButton;
			removeButton.Enabled = hasRemoveButton;
			_section = section;
			_mainForm = form;
		}

		protected override void WndProc(ref Message m)
		{
			if ((Parent as Popup).ProcessResizing(ref m))
			{
				return;
			}
			base.WndProc(ref m);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Windows.Forms.Control.CreateControl"/> method.
		/// </summary>
		protected override void OnCreateControl()
		{
			BackColor = SkinManager.GetApplicationBackgroundColor();
		}

		private void removeButton_Click(object sender, EventArgs e)
		{
			_mainForm.removeSectionFromCandidateSchedule(_section);
			this.Dispose();
		}

		private void Prerequisite_Paint(object sender, PaintEventArgs e)
		{
			((MaterialLabel)sender).ForeColor = ColorExtension.ToColor((int)Primary.Red500);
		}

		private void CoursePopup_Paint(object sender, PaintEventArgs e)
		{
			_deleteImage.DocumentElement.Attributes["fill"].Value = "#" + SkinManager.ColorScheme.TextColor.R.ToString("X2") + SkinManager.ColorScheme.TextColor.G.ToString("X2") + SkinManager.ColorScheme.TextColor.B.ToString("X2");
			removeButton.Icon = SvgDocument.Open(_deleteImage).Draw(24, 24);
		}
	}
}