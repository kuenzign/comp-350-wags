﻿namespace WaGS.UI
{
    partial class CoursePopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.removeButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.timeLabel = new MaterialSkin.Controls.MaterialLabel();
            this.locationLabel = new MaterialSkin.Controls.MaterialLabel();
            this.nameLabel = new MaterialSkin.Controls.MaterialLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.prerequisiteLabel = new MaterialSkin.Controls.MaterialLabel();
            this.capacityLabel = new MaterialSkin.Controls.MaterialLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // removeButton
            // 
            this.removeButton.AutoSize = true;
            this.removeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.removeButton.Depth = 0;
            this.removeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.removeButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.removeButton.Icon = null;
            this.removeButton.Location = new System.Drawing.Point(357, 3);
            this.removeButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.removeButton.Name = "removeButton";
            this.removeButton.Primary = true;
            this.tableLayoutPanel1.SetRowSpan(this.removeButton, 2);
            this.removeButton.Size = new System.Drawing.Size(44, 36);
            this.removeButton.TabIndex = 3;
            this.removeButton.TextUpperCase = true;
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // timeLabel
            // 
            this.timeLabel.Depth = 0;
            this.timeLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.timeLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.timeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.timeLabel.Location = new System.Drawing.Point(3, 42);
            this.timeLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(348, 19);
            this.timeLabel.TabIndex = 2;
            // 
            // locationLabel
            // 
            this.locationLabel.Depth = 0;
            this.locationLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.locationLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.locationLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.locationLabel.Location = new System.Drawing.Point(3, 19);
            this.locationLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(348, 19);
            this.locationLabel.TabIndex = 1;
            // 
            // nameLabel
            // 
            this.nameLabel.Depth = 0;
            this.nameLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.nameLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.nameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.nameLabel.Location = new System.Drawing.Point(3, 0);
            this.nameLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(348, 19);
            this.nameLabel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Controls.Add(this.prerequisiteLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.removeButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.nameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.locationLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.timeLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.capacityLabel, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(404, 99);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // prerequisiteLabel
            // 
            this.prerequisiteLabel.AutoSize = true;
            this.prerequisiteLabel.Depth = 0;
            this.prerequisiteLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.prerequisiteLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.prerequisiteLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.prerequisiteLabel.Location = new System.Drawing.Point(3, 80);
            this.prerequisiteLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.prerequisiteLabel.Name = "prerequisiteLabel";
            this.prerequisiteLabel.Size = new System.Drawing.Size(348, 19);
            this.prerequisiteLabel.TabIndex = 5;
            this.prerequisiteLabel.Paint += new System.Windows.Forms.PaintEventHandler(this.Prerequisite_Paint);
            // 
            // capacityLabel
            // 
            this.capacityLabel.AutoSize = true;
            this.capacityLabel.Depth = 0;
            this.capacityLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.capacityLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.capacityLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.capacityLabel.Location = new System.Drawing.Point(3, 61);
            this.capacityLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.capacityLabel.Name = "capacityLabel";
            this.capacityLabel.Size = new System.Drawing.Size(348, 19);
            this.capacityLabel.TabIndex = 4;
            // 
            // CoursePopup
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Roboto", 11F);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "CoursePopup";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.Size = new System.Drawing.Size(422, 117);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CoursePopup_Paint);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel nameLabel;
        private MaterialSkin.Controls.MaterialLabel locationLabel;
        private MaterialSkin.Controls.MaterialLabel timeLabel;
        private MaterialSkin.Controls.MaterialRaisedButton removeButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialLabel capacityLabel;
        private MaterialSkin.Controls.MaterialLabel prerequisiteLabel;
    }
}