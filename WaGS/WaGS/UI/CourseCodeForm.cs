﻿#region copyright

////////////////////////////////////////////////
// FileName:          CourseCodeForm.cs
// Namespace:         WaGS.UI
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WaGS.Core;

namespace WaGS.UI
{
    internal partial class CourseCodeForm : MaterialForm
    {
        public CourseCodeForm(SectionList sl, Database db, CourseList completedCourses)
            : base()
        {
            InitializeComponent();

            foreach (Section s in sl)
            {
                var item = new ListViewItem(new[] { s.Parent.Department + " " + s.Parent.CourseNumber.ToString() + " " + s.SectionLetter });
                item.Tag = s;
                this.CourseCodeDisplay.Items.Add(item);
            }
            this.CourseCodeDisplay.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            this.CourseCodeDisplay.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            Warnings.Visible = false;

            List<TimeSlot> sectionTimes = new List<TimeSlot>();
            bool requirementFound = false;
            foreach (Section sec in sl)
            {
                sectionTimes.AddRange(sec.Times);
                if (!requirementFound)
                {
                    foreach (Course requirement in sec.Parent.PrerequisiteList)
                    {
                        if (!completedCourses.Contains(requirement))
                        {
                            Warnings.Text += "You are missing prerequisites for some course(s).";
                            Warnings.Visible = true;
                            requirementFound = true;
                            break;
                        }
                    }
                }
            }

            bool timeCollision = false;
            foreach (TimeSlot time in sectionTimes)
            {
                foreach (TimeSlot nextTime in sectionTimes)
                {
                    if (time != nextTime &&
                        time.dayOfWeek == nextTime.dayOfWeek &&
                        !(time.EndTime <= nextTime.StartTime || time.StartTime >= nextTime.EndTime)
                        )
                    {
                        if (requirementFound)
                        {
                            Warnings.Text += "\n  ";
                        }
                        Warnings.Text += "You have time overlap in your schedule.";
                        Warnings.Visible = true;
                        timeCollision = true;
                        break;
                    }
                }
                if (timeCollision)
                {
                    break;
                }
            }
        }

        private void CourseCodeDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int i in ((MaterialListView)sender).SelectedIndices)
            {
                Console.Write(i.ToString());
                Console.Write(" ");
                Section s = (Section)((MaterialListView)sender).Items[i].Tag;
                Clipboard.SetText(s.Parent.Department + " " + s.Parent.CourseNumber.ToString() + " " + s.SectionLetter);
            }
        }

        private void CourseCodeDisplay_Resize(object sender, EventArgs e)
        {
            ((MaterialListView)sender).Columns[0].Width = ((MaterialListView)sender).Width - SystemInformation.VerticalScrollBarWidth;
        }

        private void Warnings_Paint(object sender, PaintEventArgs e)
        {
            ((MaterialLabel)sender).ForeColor = ColorExtension.ToColor((int)Primary.Red500);
        }
    }
}