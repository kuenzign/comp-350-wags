﻿namespace WaGS.UI
{
    partial class CompletedCoursesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchResultsListView = new MaterialSkin.Controls.MaterialListView();
            this.CourseName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CompletedCoursesListView = new MaterialSkin.Controls.MaterialListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.courseLevelComboBox = new System.Windows.Forms.ComboBox();
            this.departmentLabel = new MaterialSkin.Controls.MaterialLabel();
            this.departmentComboBox = new System.Windows.Forms.ComboBox();
            this.courseLevelLabel = new MaterialSkin.Controls.MaterialLabel();
            this.doneButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // SearchResultsListView
            // 
            this.SearchResultsListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SearchResultsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.CourseName});
            this.SearchResultsListView.Depth = 0;
            this.SearchResultsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchResultsListView.Font = new System.Drawing.Font("Roboto", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.SearchResultsListView.FullRowSelect = true;
            this.SearchResultsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.SearchResultsListView.Location = new System.Drawing.Point(3, 53);
            this.SearchResultsListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SearchResultsListView.MouseState = MaterialSkin.MouseState.OUT;
            this.SearchResultsListView.Name = "SearchResultsListView";
            this.SearchResultsListView.OwnerDraw = true;
            this.SearchResultsListView.Size = new System.Drawing.Size(280, 365);
            this.SearchResultsListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.SearchResultsListView.TabIndex = 0;
            this.SearchResultsListView.UseCompatibleStateImageBehavior = false;
            this.SearchResultsListView.View = System.Windows.Forms.View.Details;
            this.SearchResultsListView.SelectedIndexChanged += new System.EventHandler(this.SearchResultsListView_SelectedIndexChanged);
            this.SearchResultsListView.Resize += new System.EventHandler(this.SearchResultsListView_Resize);
            // 
            // CourseName
            // 
            this.CourseName.Text = "Search Results";
            this.CourseName.Width = 312;
            // 
            // CompletedCoursesListView
            // 
            this.CompletedCoursesListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CompletedCoursesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.CompletedCoursesListView.Depth = 0;
            this.CompletedCoursesListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletedCoursesListView.Font = new System.Drawing.Font("Roboto", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.CompletedCoursesListView.FullRowSelect = true;
            this.CompletedCoursesListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.CompletedCoursesListView.Location = new System.Drawing.Point(289, 53);
            this.CompletedCoursesListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.CompletedCoursesListView.MouseState = MaterialSkin.MouseState.OUT;
            this.CompletedCoursesListView.Name = "CompletedCoursesListView";
            this.CompletedCoursesListView.OwnerDraw = true;
            this.CompletedCoursesListView.Size = new System.Drawing.Size(280, 365);
            this.CompletedCoursesListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.CompletedCoursesListView.TabIndex = 1;
            this.CompletedCoursesListView.UseCompatibleStateImageBehavior = false;
            this.CompletedCoursesListView.View = System.Windows.Forms.View.Details;
            this.CompletedCoursesListView.SelectedIndexChanged += new System.EventHandler(this.CompletedCoursesListView_SelectedIndexChanged);
            this.CompletedCoursesListView.Resize += new System.EventHandler(this.CompletedCoursesListView_Resize);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Completed Courses";
            this.columnHeader1.Width = 366;
            // 
            // courseLevelComboBox
            // 
            this.courseLevelComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.courseLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.courseLevelComboBox.FormattingEnabled = true;
            this.courseLevelComboBox.Items.AddRange(new object[] {
            "-",
            "100-199",
            "200-299",
            "300-399",
            "400+"});
            this.courseLevelComboBox.Location = new System.Drawing.Point(303, 12);
            this.courseLevelComboBox.Margin = new System.Windows.Forms.Padding(3, 12, 3, 0);
            this.courseLevelComboBox.Name = "courseLevelComboBox";
            this.courseLevelComboBox.Size = new System.Drawing.Size(79, 21);
            this.courseLevelComboBox.TabIndex = 17;
            this.courseLevelComboBox.SelectedIndexChanged += new System.EventHandler(this.filterSearchResults);
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Depth = 0;
            this.departmentLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.departmentLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.departmentLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.departmentLabel.Location = new System.Drawing.Point(10, 0);
            this.departmentLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(87, 44);
            this.departmentLabel.TabIndex = 15;
            this.departmentLabel.Text = "Department";
            this.departmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // departmentComboBox
            // 
            this.departmentComboBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.departmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.departmentComboBox.FormattingEnabled = true;
            this.departmentComboBox.Items.AddRange(new object[] {
            "-",
            "ABRD",
            "ACCT",
            "ART",
            "ASTR",
            "BIOL",
            "BUSA",
            "CHEM",
            "CHIN",
            "COMM",
            "COMP",
            "ECON",
            "EDUC",
            "ELEE",
            "ENGL",
            "ENTR",
            "ENGR",
            "EXER",
            "FREN",
            "GEOL",
            "GERM",
            "GOBL",
            "GREK",
            "HIST",
            "HUMA",
            "LATN",
            "LEGL",
            "MATH",
            "MECE",
            "MUSI",
            "PHIL",
            "PHYE",
            "PHYS",
            "POLS",
            "PSYC",
            "RELI",
            "SCIC",
            "SEDU",
            "SOCI",
            "SOCW",
            "SPAN",
            "SSFT",
            "THEA",
            "WRIT"});
            this.departmentComboBox.Location = new System.Drawing.Point(103, 12);
            this.departmentComboBox.Margin = new System.Windows.Forms.Padding(3, 12, 3, 0);
            this.departmentComboBox.Name = "departmentComboBox";
            this.departmentComboBox.Size = new System.Drawing.Size(78, 21);
            this.departmentComboBox.TabIndex = 14;
            this.departmentComboBox.SelectedIndexChanged += new System.EventHandler(this.filterSearchResults);
            // 
            // courseLevelLabel
            // 
            this.courseLevelLabel.AutoSize = true;
            this.courseLevelLabel.Depth = 0;
            this.courseLevelLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.courseLevelLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.courseLevelLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.courseLevelLabel.Location = new System.Drawing.Point(201, 0);
            this.courseLevelLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.courseLevelLabel.Name = "courseLevelLabel";
            this.courseLevelLabel.Size = new System.Drawing.Size(96, 44);
            this.courseLevelLabel.TabIndex = 16;
            this.courseLevelLabel.Text = "Course Level";
            this.courseLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // doneButton
            // 
            this.doneButton.AutoSize = true;
            this.doneButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.doneButton.Depth = 0;
            this.doneButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.doneButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.doneButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.doneButton.Icon = null;
            this.doneButton.Location = new System.Drawing.Point(415, 3);
            this.doneButton.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.doneButton.MaximumSize = new System.Drawing.Size(150, 65);
            this.doneButton.MinimumSize = new System.Drawing.Size(150, 32);
            this.doneButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.doneButton.Name = "doneButton";
            this.doneButton.Padding = new System.Windows.Forms.Padding(5);
            this.doneButton.Primary = true;
            this.doneButton.Size = new System.Drawing.Size(150, 38);
            this.doneButton.TabIndex = 20;
            this.doneButton.Text = "Done";
            this.doneButton.TextUpperCase = true;
            this.doneButton.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.CompletedCoursesListView, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.SearchResultsListView, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 73);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(572, 421);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 181F));
            this.tableLayoutPanel2.Controls.Add(this.departmentLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.departmentComboBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.doneButton, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.courseLevelComboBox, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.courseLevelLabel, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(566, 44);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // CompletedCoursesForm
            // 
            this.AcceptButton = this.doneButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 503);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CompletedCoursesForm";
            this.Padding = new System.Windows.Forms.Padding(9, 73, 9, 9);
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Completed Courses";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialListView SearchResultsListView;
        private System.Windows.Forms.ColumnHeader CourseName;
        private MaterialSkin.Controls.MaterialListView CompletedCoursesListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ComboBox courseLevelComboBox;
        private MaterialSkin.Controls.MaterialLabel departmentLabel;
        private System.Windows.Forms.ComboBox departmentComboBox;
        private MaterialSkin.Controls.MaterialLabel courseLevelLabel;
        private MaterialSkin.Controls.MaterialRaisedButton doneButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}