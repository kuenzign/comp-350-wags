﻿#region copyright

////////////////////////////////////////////////
// FileName:          MainForm.cs
// Namespace:         WaGS.UI
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using MaterialSkin;
using MaterialSkin.Controls;
using PopupControl;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WaGS.Core;
using WindowsFormsCalendar;

namespace WaGS.UI
{
    public partial class MainForm : MaterialForm
    {
        #region MEMBER VARIABLES

        private MaterialSkinManager _materialSkinManager;
        private ColorScheme _lightColorScheme, _darkColorScheme;
        private Database _database;
        private SectionList _candidateSchedule;
        private CourseList _completedCourses;
        private Popup _coursePopup;
        private Popup _hamburgerMenu;
        private Search _search;
        internal XmlDocument _conflictImage;
        internal XmlDocument _menuImage;
        private Queue<Popup> _warningQueue;
        private Thread _searchThread;
        private Thread _warningThread;

        public enum UITheme { Light, Dark };

        private UITheme theme;

        public UITheme Theme
        {
            get { return theme; }
            set
            {
                theme = value;
                if (theme == UITheme.Light)
                {
                    _materialSkinManager.ColorScheme = _lightColorScheme;
                    _materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                }
                if (theme == UITheme.Dark)
                {
                    _materialSkinManager.ColorScheme = _darkColorScheme;
                    _materialSkinManager.Theme = MaterialSkinManager.Themes.DARK;
                }
            }
        }

        private const int WS_HSCROLL = 0x100000;
        private const int WS_VSCROLL = 0x200000;
        private const int GWL_STYLE = (-16);

        [DllImport("user32", CharSet = CharSet.Auto)]
        private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

        internal static bool IsVerticalScrollBarVisible(Control ctrl)
        {
            if (!ctrl.IsHandleCreated)
                return false;

            return (GetWindowLong(ctrl.Handle, GWL_STYLE) & WS_VSCROLL) != 0;
        }

        internal static bool IsHorizontalScrollBarVisible(Control ctrl)
        {
            if (!ctrl.IsHandleCreated)
                return false;

            return (GetWindowLong(ctrl.Handle, GWL_STYLE) & WS_HSCROLL) != 0;
        }

        #endregion MEMBER VARIABLES

        #region CONSTRUCTOR/DESTRUCTOR

        public MainForm()
        {
            _candidateSchedule = new SectionList();
            _database = new Database();
            _database.load();

            _warningQueue = new Queue<Popup>();

            _completedCourses = new CourseList();
            _completedCourses.open(Constants.COMPLETED_FILE_NAME, _database);

            InitializeComponent();
            theme = UITheme.Light;
            _materialSkinManager = MaterialSkinManager.Instance;
            _materialSkinManager.AddFormToManage(this);
            _materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            _lightColorScheme = new ColorScheme(Primary.Green700, Primary.Green800, Primary.Green500, Accent.Green100, TextShade.WHITE);
            _darkColorScheme = new ColorScheme(Primary.Purple700, Primary.Purple800, Primary.Purple500, Accent.Purple100, TextShade.WHITE);
            _materialSkinManager.ColorScheme = _lightColorScheme;

            switch (calendarView.ViewStart.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    calendarView.ViewStart = calendarView.ViewStart.AddDays(1);
                    break;

                case DayOfWeek.Monday:
                    break;

                case DayOfWeek.Tuesday:
                    calendarView.ViewStart = calendarView.ViewStart.AddDays(-1);
                    break;

                case DayOfWeek.Wednesday:
                    calendarView.ViewStart = calendarView.ViewStart.AddDays(-2);
                    break;

                case DayOfWeek.Thursday:
                    calendarView.ViewStart = calendarView.ViewStart.AddDays(-3);
                    break;

                case DayOfWeek.Friday:
                    calendarView.ViewStart = calendarView.ViewStart.AddDays(-4);
                    break;

                case DayOfWeek.Saturday:
                    calendarView.ViewStart = calendarView.ViewStart.AddDays(-5);
                    break;
            }
            calendarView.ViewEnd = calendarView.ViewStart.AddDays(4);

            _conflictImage = new XmlDocument();
            _conflictImage.LoadXml(Properties.Resources.conflict);
            _menuImage = new XmlDocument();
            _menuImage.LoadXml(Properties.Resources.menu);

            //Setting the ComboBoxes at the top of the window to display "-" initially
            departmentComboBox.Items.Clear();
            departmentComboBox.Items.Add("-");
            departmentComboBox.Items.AddRange(_database.getDepartments());
            departmentComboBox.SelectedIndex = 0;

            courseLevelComboBox.SelectedIndex = 0;
            startTimeComboBox.SelectedIndex = 0;
            endTimeComboBox.SelectedIndex = 0;

            _hamburgerMenu = new Popup(new HamburgerMenu(this, MenuButton));
            _hamburgerMenu.DropShadowEnabled = false;
            _hamburgerMenu.FocusOnOpen = true;
            _hamburgerMenu.AutoClose = false;
            _hamburgerMenu.ShowingAnimation = PopupAnimations.LeftToRight | PopupAnimations.Slide;
            _hamburgerMenu.HidingAnimation = PopupAnimations.RightToLeft | PopupAnimations.Slide;
            _hamburgerMenu.TopLevel = false;
            _hamburgerMenu.Parent = this;
            _hamburgerMenu.BringToFront();
        }

        ~MainForm()
        {
            if (_searchThread != null)
                _searchThread.Abort();
            if (_warningThread != null)
                _warningThread.Abort();
            _searchThread.Join();
            _warningThread.Join();
        }

        #endregion CONSTRUCTOR/DESTRUCTOR

        #region HAMBURGER

        //Toggles the Hamburger menu when you click on the button
        private void MenuButton_Click(object sender, EventArgs e)
        {
            if (MenuButton.IsMenuOpen)
            {
                _hamburgerMenu.Height = Height - Padding.Top;
                _hamburgerMenu.Show(0, Padding.Top);
            }
            else
            {
                _hamburgerMenu.Close();
            }
        }

        //Creates an open file dialog to open a previously saved schedule and makes it the new schedule
        public void ClickOpen(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Open Schedule";
            ofd.Filter = Constants.FILE_DIALOG_FILTER;

            //open the file
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (Section sec in _candidateSchedule.ToList())
                {
                    removeSectionFromCandidateSchedule(sec);
                }
                _candidateSchedule.open(ofd.FileName, _database);
            }

            foreach (Section s in _candidateSchedule)
            {
                addToCalendar(s);
            }
            refreshCandidateSchedule();
            liveUpdateSearch(null, null);
        }

        //Creates a save file dialog to save the current schedul to a file
        public void ClickSave(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Save Schedule As";
            sfd.Filter = Constants.FILE_DIALOG_FILTER;

            //Saves a csv file of the candidate schedule (course department and number)
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                _candidateSchedule.save(sfd.FileName);
            }
        }

        //Shows the CourseCodes dialog allowing you to easily copy your course codes to myGCC
        public void ClickShowCourseCodes(object sender, EventArgs e)
        {
            CourseCodeForm codesForm = new CourseCodeForm(_candidateSchedule, _database, _completedCourses);
            _materialSkinManager.AddFormToManage(codesForm);
            codesForm.ShowDialog(this);
        }

        //Shows the about page for this project
        public void ClickAbout(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            _materialSkinManager.AddFormToManage(aboutForm);
            aboutForm.ShowDialog(this);
        }

        //Toggles between the light and dark themes
        public void ClickTheme(object sender, EventArgs e)
        {
            if (Theme == UITheme.Light)
            {
                Theme = UITheme.Dark;
                ((Control)sender).Text = "Light Theme";
            }
            else if (Theme == UITheme.Dark)
            {
                Theme = UITheme.Light;
                ((Control)sender).Text = "Dark Theme";
            }
        }

        #endregion HAMBURGER

        #region CALENDAR

        //Adds a course to the calendar and displays conflict information
        private void addToCalendar(Section sec)
        {
            foreach (TimeSlot ts in sec.Times)
            {
                String name = sec.Parent.Department + " " + sec.Parent.CourseNumber.ToString() + " " + sec.SectionLetter;
                WindowsFormsCalendar.CalendarItem c = new WindowsFormsCalendar.CalendarItem(calendarView, ts.StartTime, ts.EndTime, name);

                //for each item in the calendar items collection...
                foreach (CalendarItem item in c.DayStart.ContainedItems)
                {
                    //if the times overlap...
                    if (c.StartDate.TimeOfDay <= item.EndDate.TimeOfDay && c.EndDate.TimeOfDay >= item.StartDate.TimeOfDay)
                    {
                        //set the color of the conflicting events to blue...
                        c.Pattern = System.Drawing.Drawing2D.HatchStyle.WideUpwardDiagonal;
                        c.PatternColor = ColorExtension.ToColor((int)Primary.Red500);
                        c.SvgImage = _conflictImage;
                        c.ImageAlign = CalendarItemImageAlign.East;

                        item.Pattern = System.Drawing.Drawing2D.HatchStyle.WideUpwardDiagonal;
                        item.PatternColor = ColorExtension.ToColor((int)Primary.Red500);
                        item.SvgImage = _conflictImage;
                        item.ImageAlign = CalendarItemImageAlign.East;
                    }
                }

                c.Tag = sec;
                calendarView.Items.Add(c);
            }
        }

        //Displays course popup that allows you to remove a course after clicking a course in the calendar
        private void calendarView_ItemClick(object sender, CalendarItemEventArgs e)
        {
            if (_coursePopup != null)
            {
                _coursePopup.Close();
                _coursePopup.Dispose();
                _coursePopup = null;
            }
            _coursePopup = new Popup(new CoursePopup(this, (Section)e.Item.Tag, true, _completedCourses));
            _coursePopup.AutoSize = true;
            _coursePopup.FocusOnOpen = true;
            _coursePopup.ShowingAnimation = PopupAnimations.TopToBottom | PopupAnimations.Roll;
            _coursePopup.HidingAnimation = PopupAnimations.BottomToTop | PopupAnimations.Roll;
            _coursePopup.Show(e.Item.Calendar, e.Item.BoxBounds.Left - 1, e.Item.BoxBounds.Bottom - 1);
        }

        //Refreshes everything in the calendar
        private void refreshCalendar()
        {
            calendarView.Items.Clear();
            foreach (CalendarDay day in calendarView.Days)
            {
                day.ContainedItems.Clear();
            }
            foreach (Section sec in _candidateSchedule)
            {
                addToCalendar(sec);
            }
        }

        #endregion CALENDAR

        #region CANDIDATE SCHEDULE

        //Refreshes the display of the candidate schedule
        private void refreshCandidateSchedule()
        {
            CandidateScheduleListView.Items.Clear();
            foreach (Section sec in _candidateSchedule)
            {
                var item = new ListViewItem(new[] { sec.Parent.Department + " " + sec.Parent.CourseNumber.ToString() + " " + sec.SectionLetter });
                item.Tag = sec;
                CandidateScheduleListView.Items.Add(item);
            }
        }

        //Removes a section from the candidate schedule list and calendar
        internal void removeSectionFromCandidateSchedule(Section sec)
        {
            _candidateSchedule.Remove(sec);
            if (_search.SearchResults.Find(x => x.Sections.Contains(sec)) != null)
                SearchResultListView.Items.Add(sectionToListViewItem(sec));

            refreshCalendar();
            //rewrites the Candidate List
            refreshCandidateSchedule();
            liveUpdateSearch(null, null);
            _warningQueue.Clear();
            notifyUnmetPrerequisitesFrom(_candidateSchedule);
        }

        //Auto sizes CandidateScheduleListView depending on if it has a scrollbar or not
        private void CandidateScheduleListView_Resize(object sender, EventArgs e)
        {
            if (IsVerticalScrollBarVisible(((MaterialListView)sender).Parent))
            {
                ((MaterialListView)sender).Columns[0].Width = ((MaterialListView)sender).Parent.Width - SystemInformation.VerticalScrollBarWidth;
            }
            else
            {
                ((MaterialListView)sender).Columns[0].Width = ((MaterialListView)sender).Width;
            }
        }

        //Displays course popup that allows you to remove a course after clicking a course in the CandidateScheduleListView
        private void CandidateScheduleListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int i in CandidateScheduleListView.SelectedIndices)
            {
                if (_coursePopup != null)
                {
                    _coursePopup.Close();
                    _coursePopup.Dispose();
                    _coursePopup = null;
                }
                _coursePopup = new Popup(new CoursePopup(this, (Section)CandidateScheduleListView.Items[i].Tag, true, _completedCourses));
                _coursePopup.ShowingAnimation = PopupAnimations.TopToBottom | PopupAnimations.Roll;
                _coursePopup.HidingAnimation = PopupAnimations.None;
                _coursePopup.HidingAnimation = PopupAnimations.BottomToTop | PopupAnimations.Roll;
                _coursePopup.Show(CandidateScheduleListView, CandidateScheduleListView.Items[i].Bounds.Left, CandidateScheduleListView.Items[i].Bounds.Bottom);
            }
        }

        private void ClearCoursesButton_Click(object sender, EventArgs e)
        {
            _warningQueue.Clear();
            _candidateSchedule.Clear();
            liveUpdateSearch(null, null);
            calendarView.Items.Clear();
            CandidateScheduleListView.Items.Clear();
        }

        #endregion CANDIDATE SCHEDULE

        #region SEARCH

        //Gets a new list of courses filtered based on the filters applied from the dropdowns
        private void filterSearchResults(object sender = null, EventArgs e = null)
        {
            //An instance of search that is given everything necessary to make a query
            _search = new Search(departmentComboBox.Text, courseLevelComboBox.Text, startTimeComboBox.Text, endTimeComboBox.Text, searchBar.Text);
            //Returns a query in the form of "<string>department,<string>courselevel"...

            _search.SearchResults = _database.query(_search.generateQuery());

            //Some testing of Search, which sets the search bar to be either the department selected or the courselevel selected...

            //NOTE: There is a struct called query in Search.cs, which defines what a query is...
            //This can easily be changed to include search parameters besides department and course level...

            CultureInfo culture = CultureInfo.CurrentCulture;
            SearchResultListView.Items.Clear();
            List<ListViewItem> tmp = new List<ListViewItem>();
            foreach (Course c in _search.SearchResults)
            {
                foreach (Section s in c.Sections)
                {
                    tmp.Add(sectionToListViewItem(s));
                }
            }
            liveUpdateSearch(null, null);
        }

        //When an item is clicked inside the SearchResultListView removes it from the list and adds it to the candidate schedule and calendar
        private void SearchResultListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int i in SearchResultListView.SelectedIndices)
            {
                //if the course that was clicked on is not already in the candidate schedule
                Section sec = (Section)SearchResultListView.Items[i].Tag;
                if (!_candidateSchedule.Contains(sec))
                {
                    addToCalendar(sec);
                    _candidateSchedule.Add(sec);
                    refreshCandidateSchedule();
                    if (unmetPrerequisites(sec))
                        CreateWarning(sec);
                }

                SearchResultListView.Items.Remove(SearchResultListView.Items[i]);
            }
        }

        //Displays course popup when hovering over a course in the SearchResultListView
        private void SearchResultListView_ItemMouseHover(object sender, ListViewItemMouseHoverEventArgs e)
        {
            if (_coursePopup != null)
            {
                _coursePopup.Close();
                _coursePopup.Dispose();
                _coursePopup = null;
            }
            _coursePopup = new Popup(new CoursePopup(this, (Section)e.Item.Tag, false, _completedCourses));
            _coursePopup.ShowingAnimation = PopupAnimations.Blend;
            _coursePopup.HidingAnimation = PopupAnimations.None;
            _coursePopup.Show(SearchResultListView, e.Item.Bounds.Left, e.Item.Bounds.Bottom);
            System.Drawing.Rectangle rect = SearchResultListView.GetItemRect(e.Item.Index);
        }

        //When the mouse leaves the SearchResultListView deletes the current course popup
        private void SearchResultListView_MouseLeave(object sender, EventArgs e)
        {
            if (_coursePopup != null)
            {
                _coursePopup.Hide();
                _coursePopup.Dispose();
                _coursePopup = null;
            }
        }

        //Function called when the search bar text is changed and sorts on a second thread
        private void liveUpdateSearch(object sender, EventArgs e)
        {
            if (_searchThread != null && _searchThread.IsAlive)
            {
                _searchThread.Abort();
                _searchThread.Join();
            }
            _searchThread = new Thread(new ThreadStart(sortSearchResults));
            _searchThread.Start();
            while (!_searchThread.IsAlive) { }
        }

        //Sorts and filters results list based off input from the the search bar
        private void sortSearchResults()
        {
            timer1.Stop();
            List<ListViewItem> sortedCourses = new List<ListViewItem>();
            if (searchBar.Text.Length == 0)
            {
                foreach (Course cs in _search.SearchResults)
                {
                    foreach (Section sec in cs.Sections)
                    {
                        if (!_candidateSchedule.Contains(sec))
                            sortedCourses.Add(sectionToListViewItem(sec));
                    }
                }
            }
            else
            {
                SectionList sl = new SectionList();
                foreach (Course cs in _search.SearchResults)
                {
                    sl.AddRange(cs.Sections);
                }
                Section[] sections = sl.ToArray();

                //Makes an array for tracking the priorities of each SearchResultListView item
                int[] priorities = new int[sections.Length];

                //An array to hold each of the courses in the listview, so that array.sort can be used
                ListViewItem[] crsArr = new ListViewItem[sections.Length];

                //For each item in the searchResultListView
                for (int index = 0; index < sections.Length; index++)
                {
                    //Get the row currently being considered as a string (Only Dept section and course no.)
                    Section le = sections[index];
                    string listEntry = le.Parent.LongTitle.ToString().ToUpper();

                    //Find the levenshtein distance between the search and the item being considered in the listview
                    priorities[index] = _search.LevenshteinDistance(listEntry.ToUpper(), searchBar.Text.ToUpper());
                    if (listEntry.Length - searchBar.Text.Length > 0)
                    {
                        priorities[index] -= listEntry.Length - searchBar.Text.Length;
                    }

                    //Determines if the current entry contains the exact substring matching the search bar, and adjusts priority accordingly
                    if (searchBar.Text.Length > 1 && listEntry.Contains(searchBar.Text))
                    {
                        priorities[index] -= Constants.LEVENSTEIN_EXACT_MATCH_MODIFIER;
                    }
                    //If the search is not empty
                    //If the first character is the same, adjust the priority...
                    if (searchBar.Text.Length > 0 && listEntry.Length > 0 && listEntry[0] == searchBar.Text.ToUpper()[0])
                    {
                        priorities[index] -= Constants.LEVENSTEIN_FIRST_CHAR_MATCH_MODIFIER;
                    }
                    crsArr[index] = sectionToListViewItem(le);
                }
                //Remove all the courses in the list view so that the sorted ones can be added

                //Sort both the priorities and the crsArr the same way, based on the priorities...
                Array.Sort(priorities, crsArr);

                //Get the lowest priority match
                int lowest = int.MaxValue - Constants.LEVENSTEIN_CUTOFF;
                if (priorities.Length > 0)
                    lowest = priorities[0];

                //Adds the sorted courses back to the list that is displayed
                for (int addLoop = 0; addLoop < crsArr.Length; addLoop++)
                {
                    if (priorities[addLoop] <= lowest + Constants.LEVENSTEIN_CUTOFF)
                    {
                        sortedCourses.Add(crsArr[addLoop]);
                    }
                }
            }
            sortedCourses.RemoveAll(x => _candidateSchedule.Contains((Section)x.Tag));
            Invoke((MethodInvoker)delegate
            {
                SearchResultListView.Items.Clear();
                SearchResultListView.Items.AddRange(sortedCourses.ToArray());
                SearchResultListView.Refresh();
            });
        }

        #endregion SEARCH

        #region PREREQUISITES

        //Returns true if there are prerequisites for the section not in completedCourses
        private bool unmetPrerequisites(Section sec)
        {
            foreach (Course requirement in sec.Parent.PrerequisiteList)
            {
                if (!_completedCourses.Contains(requirement))
                {
                    return true;
                }
            }
            return false;
        }

        //Checks all prerequisites for classes in the candidate schedule and adds a warning if any are missing
        private void notifyUnmetPrerequisitesFrom(SectionList sec)
        {
            foreach (Section currentSec in sec)
            {
                if (unmetPrerequisites(currentSec))
                {
                    CreateWarning(currentSec);
                }
            }
        }

        private void setCoursesTakenButton_Click(object sender, EventArgs e)
        {
            CompletedCoursesForm completedCoursesForm = new CompletedCoursesForm(_database, _completedCourses);
            _materialSkinManager.AddFormToManage(completedCoursesForm);
            completedCoursesForm.ShowDialog(this);
            _completedCourses.save(Constants.COMPLETED_FILE_NAME);
        }

        #endregion PREREQUISITES

        #region WARNING NOTIFICATION

        //Creates a warning and adds it to the warningQueue and displays the warnings on a second thread
        private void CreateWarning(Section sec)
        {
            MaterialLabel warningLabel = new MaterialLabel();
            warningLabel.BackColor = SkinManager.GetApplicationBackgroundColor();
            warningLabel.ForeColor = ColorExtension.ToColor((int)Primary.Red500);

            warningLabel.Paint += (object sender, PaintEventArgs e) =>
            {
                ((MaterialLabel)sender).ForeColor = ColorExtension.ToColor((int)Primary.Red500);
            };

            warningLabel.Text = "You have unmet prerequisites for " + sec.Parent.Department + " " + sec.Parent.CourseNumber.ToString() + " " + sec.SectionLetter + ".";
            warningLabel.MinimumSize = System.Drawing.Size.Round(CreateGraphics().MeasureString(warningLabel.Text, SkinManager.ROBOTO_MEDIUM_11));

            Popup warningPopup = new Popup(warningLabel);
            warningPopup.ShowingAnimation = PopupAnimations.BottomToTop | PopupAnimations.Slide;
            warningPopup.HidingAnimation = PopupAnimations.TopToBottom | PopupAnimations.Slide;
            warningPopup.AutoClose = false;
            warningPopup.AnimationDuration = Constants.PREREQUISITE_NOTIFICATION_ANIMATION_TIME;
            warningPopup.Tag = sec;

            warningPopup.TopLevel = false;
            warningPopup.Parent = this;

            _warningQueue.Enqueue(warningPopup);

            if (_warningThread == null || !_warningThread.IsAlive)
            {
                _warningThread = new Thread(new ThreadStart(DisplayWarning));
                _warningThread.Start();
            }
        }

        //Displays all warnings in warningQueue one at a time
        private void DisplayWarning()
        {
            while (_warningQueue.Count > 0)
            {
                Popup current = _warningQueue.Dequeue();
                if (this.IsHandleCreated)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        current.BringToFront();
                        current.Show(Width - current.Width, Height - current.Height);
                    });
                }
                else
                {
                    Thread.CurrentThread.Abort();
                }
                Task.Delay(Constants.PREREQUISITE_NOTIFICATION_DISPLAY_TIME).Wait();
                if (this.IsHandleCreated)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        current.Hide();
                    });
                }
                else
                {
                    Thread.CurrentThread.Abort();
                }
                Task.Delay(Constants.PREREQUISITE_NOTIFICATION_ANIMATION_TIME).Wait();
                if (this.IsHandleCreated)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        current.Dispose();
                    });
                }
                else
                {
                    Thread.CurrentThread.Abort();
                }
            }
        }

        #endregion WARNING NOTIFICATION

        #region MISC FUNCTIONS

        private ListViewItem sectionToListViewItem(Section sec)
        {
            string timeString;

            string Days = "";
            foreach (TimeSlot ts in sec.Times)
            {
                Days += ts.dayOfWeek;
            }

            if (sec.Times.Count > 0)
            {
                timeString = Days + " " + sec.Times[0].StartTime.ToShortTimeString() + " - " + sec.Times[0].EndTime.ToShortTimeString();
            }
            else
            {
                timeString = "No Time";
            }
            var item = new ListViewItem(new[] { sec.Parent.Department + " " + sec.Parent.CourseNumber.ToString() + " " + sec.SectionLetter, timeString });
            item.Tag = sec;
            return item;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (MenuButton.IsMenuOpen)
            {
                _hamburgerMenu.Height = Height - Padding.Top;
            }
        }

        //Called when the text of the search bar is changed...

        //Note: When the timer finishes, it executes its elapsed event as set in the designer (liveUpdateSearch)
        private void resetTimer(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Start();
        }

        #endregion MISC FUNCTIONS
    }
}