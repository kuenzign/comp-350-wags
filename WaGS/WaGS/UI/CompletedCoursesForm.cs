﻿#region copyright

////////////////////////////////////////////////
// FileName:          CompletedCoursesForm.cs
// Namespace:         WaGS.UI
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WaGS.Core;

namespace WaGS.UI
{
    internal partial class CompletedCoursesForm : MaterialForm
    {
        private Database database;
        private CourseList completedCourses;
        private Search search;

        public CompletedCoursesForm(Database db, CourseList completed)
        {
            database = db;
            completedCourses = completed;
            InitializeComponent();

            departmentComboBox.Items.Clear();
            departmentComboBox.Items.Add("-");
            departmentComboBox.SelectedIndex = 0;
            departmentComboBox.Items.AddRange(db.getDepartments());

            courseLevelComboBox.SelectedIndex = 0;
            foreach (Course c in completedCourses)
            {
                if (c != null)
                {
                    var item = new ListViewItem(new[] { c.Department + " " + c.CourseNumber.ToString() });
                    item.Tag = c;
                    CompletedCoursesListView.Items.Add(item);
                }
            }
            object sender = new object();
            EventArgs e = new EventArgs();
            filterSearchResults(sender, e);
        }

        //Gets a new list of courses filtered based on the filters applied from the dropdowns
        private void filterSearchResults(object sender = null, EventArgs e = null)
        {
            //An instance of search that is given everything necessary to make a query
            search = new Search(departmentComboBox.Text, courseLevelComboBox.Text, "-", "-", "");
            //Returns a query in the form of "<string>department,<string>courselevel"...

            search.SearchResults = database.queryPrerequisites(search.generateQuery());

            //Some testing of Search, which sets the search bar to be either the department selected or the courselevel selected...

            //NOTE: There is a struct called query in Search.cs, which defines what a query is...
            //This can easily be changed to include search parameters besides department and course level...
            SearchResultsListView.Items.Clear();
            List<ListViewItem> tmp = new List<ListViewItem>();
            foreach (Course c in search.SearchResults)
            {
                ListViewItem toAdd = new ListViewItem(new[] { c.Department + " " + c.CourseNumber.ToString() });
                toAdd.Tag = c;
                tmp.Add(toAdd);
            }
            tmp.RemoveAll(x => completedCourses.Contains((Course)x.Tag));
            SearchResultsListView.Items.AddRange(tmp.ToArray());
        }

        private void SearchResultsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int i in SearchResultsListView.SelectedIndices)
            {
                //if the course that was clicked on is not already in the candidate schedule
                if (!completedCourses.Contains((Course)SearchResultsListView.Items[i].Tag))
                {
                    Course c = (Course)SearchResultsListView.Items[i].Tag;
                    completedCourses.Add(c);
                    ListViewItem item = new ListViewItem(new[] { c.Department + " " + c.CourseNumber.ToString() });
                    item.Tag = c;
                    CompletedCoursesListView.Items.Add(item);
                }

                SearchResultsListView.Items.Remove(SearchResultsListView.Items[i]);
            }
        }

        private void CompletedCoursesListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int i in CompletedCoursesListView.SelectedIndices)
            {
                //if the course that was clicked on is not already in the candidate schedule

                Course c = (Course)CompletedCoursesListView.Items[i].Tag;
                completedCourses.Remove(c);

                var item = new ListViewItem(new[] { c.Department + " " + c.CourseNumber.ToString() });
                item.Tag = c;

                //re-add to the search results if it meets the current search results
                if (search.SearchResults.Contains(c))
                    SearchResultsListView.Items.Add(item);

                CompletedCoursesListView.Items.Remove(CompletedCoursesListView.Items[i]);
            }
        }

        private void SearchResultsListView_Resize(object sender, EventArgs e)
        {
            ((MaterialListView)sender).Columns[0].Width = ((MaterialListView)sender).Width - SystemInformation.VerticalScrollBarWidth;
        }

        private void CompletedCoursesListView_Resize(object sender, EventArgs e)
        {
            ((MaterialListView)sender).Columns[0].Width = ((MaterialListView)sender).Width - SystemInformation.VerticalScrollBarWidth;
        }
    }
}