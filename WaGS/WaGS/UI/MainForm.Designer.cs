﻿namespace WaGS.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.SearchResultListView = new MaterialSkin.Controls.MaterialListView();
            this.Courses = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.courseLevelComboBox = new System.Windows.Forms.ComboBox();
            this.setCoursesTakenButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.departmentLabel = new MaterialSkin.Controls.MaterialLabel();
            this.departmentComboBox = new System.Windows.Forms.ComboBox();
            this.endTimeComboBox = new System.Windows.Forms.ComboBox();
            this.courseLevelLabel = new MaterialSkin.Controls.MaterialLabel();
            this.startTimeComboBox = new System.Windows.Forms.ComboBox();
            this.endTimeLabel = new MaterialSkin.Controls.MaterialLabel();
            this.searchBar = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.startTimeLabel = new MaterialSkin.Controls.MaterialLabel();
            this.calendarView = new WindowsFormsCalendar.Calendar();
            this.ClearCoursesButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.CandidateScheduleListView = new MaterialSkin.Controls.MaterialListView();
            this.candidateCourses = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.MenuButton = new MaterialSkin.Controls.MaterialMenuButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // SearchResultListView
            // 
            this.SearchResultListView.AllowDrop = true;
            this.SearchResultListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SearchResultListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Courses,
            this.Time});
            this.SearchResultListView.Depth = 0;
            this.SearchResultListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchResultListView.Font = new System.Drawing.Font("Roboto", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.SearchResultListView.FullRowSelect = true;
            this.SearchResultListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.SearchResultListView.Location = new System.Drawing.Point(3, 3);
            this.SearchResultListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SearchResultListView.MouseState = MaterialSkin.MouseState.OUT;
            this.SearchResultListView.MultiSelect = false;
            this.SearchResultListView.Name = "SearchResultListView";
            this.SearchResultListView.OwnerDraw = true;
            this.tableLayoutPanel2.SetRowSpan(this.SearchResultListView, 3);
            this.SearchResultListView.Size = new System.Drawing.Size(294, 498);
            this.SearchResultListView.TabIndex = 2;
            this.SearchResultListView.UseCompatibleStateImageBehavior = false;
            this.SearchResultListView.View = System.Windows.Forms.View.Details;
            this.SearchResultListView.ItemMouseHover += new System.Windows.Forms.ListViewItemMouseHoverEventHandler(this.SearchResultListView_ItemMouseHover);
            this.SearchResultListView.SelectedIndexChanged += new System.EventHandler(this.SearchResultListView_SelectedIndexChanged);
            this.SearchResultListView.MouseLeave += new System.EventHandler(this.SearchResultListView_MouseLeave);
            // 
            // Courses
            // 
            this.Courses.Text = "Course";
            this.Courses.Width = 97;
            // 
            // Time
            // 
            this.Time.Text = "Time";
            this.Time.Width = 208;
            // 
            // courseLevelComboBox
            // 
            this.courseLevelComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.courseLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.courseLevelComboBox.FormattingEnabled = true;
            this.courseLevelComboBox.Items.AddRange(new object[] {
            "-",
            "100-199",
            "200-299",
            "300-399",
            "400+"});
            this.courseLevelComboBox.Location = new System.Drawing.Point(287, 3);
            this.courseLevelComboBox.Name = "courseLevelComboBox";
            this.courseLevelComboBox.Size = new System.Drawing.Size(78, 21);
            this.courseLevelComboBox.TabIndex = 7;
            this.courseLevelComboBox.SelectedIndexChanged += new System.EventHandler(this.filterSearchResults);
            // 
            // setCoursesTakenButton
            // 
            this.setCoursesTakenButton.AutoSize = true;
            this.setCoursesTakenButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.setCoursesTakenButton.Depth = 0;
            this.setCoursesTakenButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setCoursesTakenButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.setCoursesTakenButton.Icon = null;
            this.setCoursesTakenButton.Location = new System.Drawing.Point(975, 3);
            this.setCoursesTakenButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.setCoursesTakenButton.Name = "setCoursesTakenButton";
            this.setCoursesTakenButton.Primary = true;
            this.tableLayoutPanel3.SetRowSpan(this.setCoursesTakenButton, 2);
            this.setCoursesTakenButton.Size = new System.Drawing.Size(154, 48);
            this.setCoursesTakenButton.TabIndex = 14;
            this.setCoursesTakenButton.Text = "Set Courses Taken";
            this.setCoursesTakenButton.TextUpperCase = true;
            this.setCoursesTakenButton.UseVisualStyleBackColor = true;
            this.setCoursesTakenButton.Click += new System.EventHandler(this.setCoursesTakenButton_Click);
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Depth = 0;
            this.departmentLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.departmentLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.departmentLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.departmentLabel.Location = new System.Drawing.Point(5, 0);
            this.departmentLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(87, 27);
            this.departmentLabel.TabIndex = 5;
            this.departmentLabel.Text = "Department";
            this.departmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // departmentComboBox
            // 
            this.departmentComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.departmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.departmentComboBox.FormattingEnabled = true;
            this.departmentComboBox.Items.AddRange(new object[] {
            "-",
            "ABRD",
            "ACCT",
            "ART",
            "ASTR",
            "BIOL",
            "BUSA",
            "CHEM",
            "CHIN",
            "COMM",
            "COMP",
            "ECON",
            "EDUC",
            "ELEE",
            "ENGL",
            "ENTR",
            "ENGR",
            "EXER",
            "FREN",
            "GEOL",
            "GERM",
            "GOBL",
            "GREK",
            "HIST",
            "HUMA",
            "LATN",
            "LEGL",
            "MATH",
            "MECE",
            "MUSI",
            "PHIL",
            "PHYE",
            "PHYS",
            "POLS",
            "PSYC",
            "RELI",
            "SCIC",
            "SEDU",
            "SOCI",
            "SOCW",
            "SPAN",
            "SSFT",
            "THEA",
            "WRIT"});
            this.departmentComboBox.Location = new System.Drawing.Point(98, 3);
            this.departmentComboBox.Name = "departmentComboBox";
            this.departmentComboBox.Size = new System.Drawing.Size(78, 21);
            this.departmentComboBox.TabIndex = 3;
            this.departmentComboBox.SelectedIndexChanged += new System.EventHandler(this.filterSearchResults);
            // 
            // endTimeComboBox
            // 
            this.endTimeComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.endTimeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.endTimeComboBox.FormattingEnabled = true;
            this.endTimeComboBox.Items.AddRange(new object[] {
            "-",
            "9:00 AM",
            "10:00 AM",
            "11:00 AM",
            "12:00 PM",
            "1:00 PM",
            "2:00 PM",
            "3:00 PM",
            "4:00 PM",
            "5:00 PM",
            "6:00 PM",
            "7:00 PM",
            "8:00 PM",
            "9:00 PM"});
            this.endTimeComboBox.Location = new System.Drawing.Point(287, 30);
            this.endTimeComboBox.Name = "endTimeComboBox";
            this.endTimeComboBox.Size = new System.Drawing.Size(78, 21);
            this.endTimeComboBox.TabIndex = 13;
            this.endTimeComboBox.SelectedIndexChanged += new System.EventHandler(this.filterSearchResults);
            // 
            // courseLevelLabel
            // 
            this.courseLevelLabel.AutoSize = true;
            this.courseLevelLabel.Depth = 0;
            this.courseLevelLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.courseLevelLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.courseLevelLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.courseLevelLabel.Location = new System.Drawing.Point(185, 0);
            this.courseLevelLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.courseLevelLabel.Name = "courseLevelLabel";
            this.courseLevelLabel.Size = new System.Drawing.Size(96, 27);
            this.courseLevelLabel.TabIndex = 6;
            this.courseLevelLabel.Text = "Course Level";
            this.courseLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // startTimeComboBox
            // 
            this.startTimeComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.startTimeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.startTimeComboBox.FormattingEnabled = true;
            this.startTimeComboBox.Items.AddRange(new object[] {
            "-",
            "8:00 AM",
            "9:00 AM",
            "10:00 AM",
            "11:00 AM",
            "12:00 PM",
            "1:00 PM",
            "2:00 PM",
            "3:00 PM",
            "4:00 PM",
            "5:00 PM",
            "6:00 PM"});
            this.startTimeComboBox.Location = new System.Drawing.Point(98, 30);
            this.startTimeComboBox.Name = "startTimeComboBox";
            this.startTimeComboBox.Size = new System.Drawing.Size(78, 21);
            this.startTimeComboBox.TabIndex = 12;
            this.startTimeComboBox.SelectedIndexChanged += new System.EventHandler(this.filterSearchResults);
            // 
            // endTimeLabel
            // 
            this.endTimeLabel.AutoSize = true;
            this.endTimeLabel.Depth = 0;
            this.endTimeLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.endTimeLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.endTimeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.endTimeLabel.Location = new System.Drawing.Point(209, 27);
            this.endTimeLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.endTimeLabel.Name = "endTimeLabel";
            this.endTimeLabel.Size = new System.Drawing.Size(72, 27);
            this.endTimeLabel.TabIndex = 11;
            this.endTimeLabel.Text = "End Time";
            this.endTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // searchBar
            // 
            this.searchBar.BackColor = System.Drawing.SystemColors.Control;
            this.searchBar.Depth = 0;
            this.searchBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBar.Hint = "Search Course Title...";
            this.searchBar.Location = new System.Drawing.Point(371, 3);
            this.searchBar.MaxLength = 32767;
            this.searchBar.MouseState = MaterialSkin.MouseState.HOVER;
            this.searchBar.Name = "searchBar";
            this.searchBar.PasswordChar = '\0';
            this.searchBar.SelectedText = "";
            this.searchBar.SelectionLength = 0;
            this.searchBar.SelectionStart = 0;
            this.searchBar.Size = new System.Drawing.Size(598, 23);
            this.searchBar.TabIndex = 8;
            this.searchBar.TabStop = false;
            this.searchBar.UseSystemPasswordChar = false;
            this.searchBar.TextChanged += new System.EventHandler(this.resetTimer);
            // 
            // startTimeLabel
            // 
            this.startTimeLabel.AutoSize = true;
            this.startTimeLabel.Depth = 0;
            this.startTimeLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.startTimeLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.startTimeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.startTimeLabel.Location = new System.Drawing.Point(13, 27);
            this.startTimeLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.startTimeLabel.Name = "startTimeLabel";
            this.startTimeLabel.Size = new System.Drawing.Size(79, 27);
            this.startTimeLabel.TabIndex = 10;
            this.startTimeLabel.Text = "Start Time";
            this.startTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // calendarView
            // 
            this.calendarView.AllowItemEdit = false;
            this.calendarView.AllowItemResize = false;
            this.calendarView.AllowNew = false;
            this.calendarView.AutoScroll = true;
            this.calendarView.Depth = 0;
            this.calendarView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarView.Font = new System.Drawing.Font("Roboto", 11F);
            this.calendarView.ItemsBackgroundColor = System.Drawing.Color.RoyalBlue;
            this.calendarView.ItemsFont = new System.Drawing.Font("Roboto", 11F);
            this.calendarView.ItemsForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.calendarView.Location = new System.Drawing.Point(303, 63);
            this.calendarView.MinimumSize = new System.Drawing.Size(800, 0);
            this.calendarView.MouseState = MaterialSkin.MouseState.HOVER;
            this.calendarView.Name = "calendarView";
            this.tableLayoutPanel2.SetRowSpan(this.calendarView, 2);
            this.calendarView.Scrollbars = WindowsFormsCalendar.CalendarScrollBars.Vertical;
            this.calendarView.Size = new System.Drawing.Size(982, 438);
            this.calendarView.TabIndex = 6;
            this.calendarView.Text = "calendar1";
            this.calendarView.TimeScale = WindowsFormsCalendar.CalendarTimeScale.SixtyMinutes;
            this.calendarView.ItemClick += new WindowsFormsCalendar.Calendar.CalendarItemEventHandler(this.calendarView_ItemClick);
            // 
            // ClearCoursesButton
            // 
            this.ClearCoursesButton.AutoSize = true;
            this.ClearCoursesButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClearCoursesButton.Depth = 0;
            this.ClearCoursesButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ClearCoursesButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.ClearCoursesButton.Icon = null;
            this.ClearCoursesButton.Location = new System.Drawing.Point(1291, 471);
            this.ClearCoursesButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.ClearCoursesButton.Name = "ClearCoursesButton";
            this.ClearCoursesButton.Primary = true;
            this.ClearCoursesButton.Size = new System.Drawing.Size(144, 30);
            this.ClearCoursesButton.TabIndex = 1;
            this.ClearCoursesButton.Text = "Clear Courses";
            this.ClearCoursesButton.TextUpperCase = true;
            this.ClearCoursesButton.UseVisualStyleBackColor = true;
            this.ClearCoursesButton.Click += new System.EventHandler(this.ClearCoursesButton_Click);
            // 
            // CandidateScheduleListView
            // 
            this.CandidateScheduleListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CandidateScheduleListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.candidateCourses});
            this.CandidateScheduleListView.Depth = 0;
            this.CandidateScheduleListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CandidateScheduleListView.Font = new System.Drawing.Font("Roboto", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.CandidateScheduleListView.FullRowSelect = true;
            this.CandidateScheduleListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.CandidateScheduleListView.Location = new System.Drawing.Point(1291, 63);
            this.CandidateScheduleListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.CandidateScheduleListView.MouseState = MaterialSkin.MouseState.OUT;
            this.CandidateScheduleListView.MultiSelect = false;
            this.CandidateScheduleListView.Name = "CandidateScheduleListView";
            this.CandidateScheduleListView.OwnerDraw = true;
            this.CandidateScheduleListView.Size = new System.Drawing.Size(144, 402);
            this.CandidateScheduleListView.TabIndex = 0;
            this.CandidateScheduleListView.UseCompatibleStateImageBehavior = false;
            this.CandidateScheduleListView.View = System.Windows.Forms.View.Details;
            this.CandidateScheduleListView.SelectedIndexChanged += new System.EventHandler(this.CandidateScheduleListView_SelectedIndexChanged);
            this.CandidateScheduleListView.Resize += new System.EventHandler(this.CandidateScheduleListView_Resize);
            // 
            // candidateCourses
            // 
            this.candidateCourses.Text = "My Courses";
            this.candidateCourses.Width = 140;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MenuButton
            // 
            this.MenuButton.AutoSize = true;
            this.MenuButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MenuButton.Depth = 0;
            this.MenuButton.Font = new System.Drawing.Font("Roboto", 12F);
            this.MenuButton.Location = new System.Drawing.Point(0, 24);
            this.MenuButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.MenuButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.MenuButton.Name = "MenuButton";
            this.MenuButton.Primary = false;
            this.MenuButton.Size = new System.Drawing.Size(96, 40);
            this.MenuButton.TabIndex = 8;
            this.MenuButton.Text = "WaGS";
            this.MenuButton.TextUpperCase = false;
            this.MenuButton.UseVisualStyleBackColor = true;
            this.MenuButton.Click += new System.EventHandler(this.MenuButton_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 400;
            this.timer1.Tick += new System.EventHandler(this.liveUpdateSearch);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.SearchResultListView, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.calendarView, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.ClearCoursesButton, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.CandidateScheduleListView, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 64);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1438, 504);
            this.tableLayoutPanel2.TabIndex = 9;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel3, 2);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel3.Controls.Add(this.setCoursesTakenButton, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.courseLevelComboBox, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.searchBar, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.departmentLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.endTimeComboBox, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.startTimeLabel, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.departmentComboBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.endTimeLabel, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.startTimeComboBox, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.courseLevelLabel, 2, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(303, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1132, 54);
            this.tableLayoutPanel3.TabIndex = 10;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1442, 570);
            this.Controls.Add(this.MenuButton);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1277, 570);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(2, 64, 2, 2);
            this.Text = "WaGS";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private WindowsFormsCalendar.Calendar calendarView;
        private System.Windows.Forms.ComboBox departmentComboBox;
        private MaterialSkin.Controls.MaterialLabel departmentLabel;
        private MaterialSkin.Controls.MaterialLabel courseLevelLabel;
        private System.Windows.Forms.ComboBox courseLevelComboBox;
        private MaterialSkin.Controls.MaterialSingleLineTextField searchBar;
        private MaterialSkin.Controls.MaterialLabel startTimeLabel;
        private MaterialSkin.Controls.MaterialLabel endTimeLabel;
        private System.Windows.Forms.ComboBox startTimeComboBox;
        private System.Windows.Forms.ComboBox endTimeComboBox;
        private MaterialSkin.Controls.MaterialRaisedButton setCoursesTakenButton;
        private MaterialSkin.Controls.MaterialListView SearchResultListView;
        private System.Windows.Forms.ColumnHeader Courses;
        private System.Windows.Forms.ColumnHeader Time;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private MaterialSkin.Controls.MaterialMenuButton MenuButton;
        private MaterialSkin.Controls.MaterialListView CandidateScheduleListView;
        private System.Windows.Forms.ColumnHeader candidateCourses;
        private MaterialSkin.Controls.MaterialRaisedButton ClearCoursesButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    }
}