﻿namespace WaGS.UI
{
    partial class CourseCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Instructions = new MaterialSkin.Controls.MaterialLabel();
            this.CourseCodeDisplay = new MaterialSkin.Controls.MaterialListView();
            this.Courses = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CloseWindow = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Warnings = new MaterialSkin.Controls.MaterialLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Instructions
            // 
            this.Instructions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Instructions.AutoSize = true;
            this.Instructions.Depth = 0;
            this.Instructions.Font = new System.Drawing.Font("Roboto", 11F);
            this.Instructions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Instructions.Location = new System.Drawing.Point(33, 0);
            this.Instructions.MouseState = MaterialSkin.MouseState.HOVER;
            this.Instructions.Name = "Instructions";
            this.Instructions.Size = new System.Drawing.Size(310, 19);
            this.Instructions.TabIndex = 0;
            this.Instructions.Text = "Click on a course to copy it to your clipboard";
            // 
            // CourseCodeDisplay
            // 
            this.CourseCodeDisplay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CourseCodeDisplay.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Courses});
            this.CourseCodeDisplay.Depth = 0;
            this.CourseCodeDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CourseCodeDisplay.Font = new System.Drawing.Font("Roboto", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.CourseCodeDisplay.FullRowSelect = true;
            this.CourseCodeDisplay.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.CourseCodeDisplay.Location = new System.Drawing.Point(3, 41);
            this.CourseCodeDisplay.MouseLocation = new System.Drawing.Point(-1, -1);
            this.CourseCodeDisplay.MouseState = MaterialSkin.MouseState.OUT;
            this.CourseCodeDisplay.Name = "CourseCodeDisplay";
            this.CourseCodeDisplay.OwnerDraw = true;
            this.CourseCodeDisplay.Size = new System.Drawing.Size(370, 230);
            this.CourseCodeDisplay.TabIndex = 1;
            this.CourseCodeDisplay.UseCompatibleStateImageBehavior = false;
            this.CourseCodeDisplay.View = System.Windows.Forms.View.Details;
            this.CourseCodeDisplay.SelectedIndexChanged += new System.EventHandler(this.CourseCodeDisplay_SelectedIndexChanged);
            this.CourseCodeDisplay.Resize += new System.EventHandler(this.CourseCodeDisplay_Resize);
            // 
            // Courses
            // 
            this.Courses.Text = "Courses";
            this.Courses.Width = 370;
            // 
            // CloseWindow
            // 
            this.CloseWindow.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CloseWindow.AutoSize = true;
            this.CloseWindow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CloseWindow.Depth = 0;
            this.CloseWindow.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseWindow.Icon = null;
            this.CloseWindow.Location = new System.Drawing.Point(126, 277);
            this.CloseWindow.MouseState = MaterialSkin.MouseState.HOVER;
            this.CloseWindow.Name = "CloseWindow";
            this.CloseWindow.Primary = true;
            this.CloseWindow.Size = new System.Drawing.Size(123, 36);
            this.CloseWindow.TabIndex = 2;
            this.CloseWindow.Text = "Close Window";
            this.CloseWindow.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.CloseWindow, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.CourseCodeDisplay, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Instructions, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Warnings, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 73);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(376, 316);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // Warnings
            // 
            this.Warnings.AutoSize = true;
            this.Warnings.Depth = 0;
            this.Warnings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Warnings.Font = new System.Drawing.Font("Roboto", 11F);
            this.Warnings.Location = new System.Drawing.Point(3, 19);
            this.Warnings.MouseState = MaterialSkin.MouseState.HOVER;
            this.Warnings.Name = "Warnings";
            this.Warnings.Size = new System.Drawing.Size(370, 19);
            this.Warnings.TabIndex = 3;
            this.Warnings.Text = "*";
            this.Warnings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Warnings.Paint += new System.Windows.Forms.PaintEventHandler(this.Warnings_Paint);
            // 
            // CourseCodeForm
            // 
            this.AcceptButton = this.CloseWindow;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 398);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CourseCodeForm";
            this.Padding = new System.Windows.Forms.Padding(9, 73, 9, 9);
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Course Codes";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel Instructions;
        private MaterialSkin.Controls.MaterialListView CourseCodeDisplay;
        private System.Windows.Forms.ColumnHeader Courses;
        private MaterialSkin.Controls.MaterialRaisedButton CloseWindow;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialLabel Warnings;
    }
}