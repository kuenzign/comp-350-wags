﻿#region copyright

////////////////////////////////////////////////
// FileName:          AboutForm.cs
// Namespace:         WaGS.UI
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using MaterialSkin.Controls;
using Svg;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace WaGS.UI
{
    partial class AboutForm : MaterialForm
    {
        internal SvgDocument _logoImage;

        public AboutForm()
        {
            InitializeComponent();
            this.Text = String.Format("About {0}", AssemblyTitle);
            this.labelProductName.Text = String.Format("{0}:   Version {1}", AssemblyProduct, AssemblyVersion);
            this.labelCopyright.Text = String.Format("{0}, {1}", AssemblyCopyright, AssemblyCompany);
            this.textBoxDescription.Rtf = Properties.Resources.Attributions;
            XmlDocument image = new XmlDocument();
            image.LoadXml(Properties.Resources.dog);
            _logoImage = SvgDocument.Open(image);
        }

        #region Assembly Attribute Accessors

        public SvgDocument AssemblyLogo
        {
            get
            {
                return _logoImage;
            }
        }

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }

        #endregion Assembly Attribute Accessors

        private void imagePanel_Resize(object sender, EventArgs e)
        {
            Panel p = (Panel)sender;
            int dim = Math.Min(p.Width, p.Height);
            p.BackgroundImage = AssemblyLogo.Draw(dim, dim);
        }

        private void textBoxDescription_Resize(object sender, EventArgs e)
        {
            this.textBoxDescription.BackColor = this.SkinManager.GetApplicationBackgroundColor();
            this.textBoxDescription.ForeColor = this.SkinManager.GetPrimaryTextColor();
        }

        private void textBoxDescription_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }
    }
}