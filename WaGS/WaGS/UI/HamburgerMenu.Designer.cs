﻿namespace WaGS.UI
{
    partial class HamburgerMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.OpenButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.SaveButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.ShowCourseCodesButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.ExitButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.AboutButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.ThemeButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.OpenButton, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.SaveButton, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ShowCourseCodesButton, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ExitButton, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.AboutButton, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.materialDivider1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.materialDivider2, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.ThemeButton, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(292, 430);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // OpenButton
            // 
            this.OpenButton.AutoSize = true;
            this.OpenButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.OpenButton.Depth = 0;
            this.OpenButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.OpenButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.OpenButton.Icon = null;
            this.OpenButton.Location = new System.Drawing.Point(4, 6);
            this.OpenButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.OpenButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Primary = false;
            this.OpenButton.Size = new System.Drawing.Size(284, 36);
            this.OpenButton.TabIndex = 0;
            this.OpenButton.Text = "Open";
            this.OpenButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.OpenButton.TextUpperCase = false;
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.AutoSize = true;
            this.SaveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SaveButton.Depth = 0;
            this.SaveButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.SaveButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.SaveButton.Icon = null;
            this.SaveButton.Location = new System.Drawing.Point(4, 54);
            this.SaveButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.SaveButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Primary = false;
            this.SaveButton.Size = new System.Drawing.Size(284, 36);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Save";
            this.SaveButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SaveButton.TextUpperCase = false;
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ShowCourseCodesButton
            // 
            this.ShowCourseCodesButton.AutoSize = true;
            this.ShowCourseCodesButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ShowCourseCodesButton.Depth = 0;
            this.ShowCourseCodesButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.ShowCourseCodesButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.ShowCourseCodesButton.Icon = null;
            this.ShowCourseCodesButton.Location = new System.Drawing.Point(4, 102);
            this.ShowCourseCodesButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.ShowCourseCodesButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.ShowCourseCodesButton.Name = "ShowCourseCodesButton";
            this.ShowCourseCodesButton.Primary = false;
            this.ShowCourseCodesButton.Size = new System.Drawing.Size(284, 36);
            this.ShowCourseCodesButton.TabIndex = 2;
            this.ShowCourseCodesButton.Text = "Show Course Codes";
            this.ShowCourseCodesButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ShowCourseCodesButton.TextUpperCase = false;
            this.ShowCourseCodesButton.UseVisualStyleBackColor = true;
            this.ShowCourseCodesButton.Click += new System.EventHandler(this.ShowCourseCodesButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.AutoSize = true;
            this.ExitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ExitButton.Depth = 0;
            this.ExitButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExitButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.ExitButton.Icon = null;
            this.ExitButton.Location = new System.Drawing.Point(4, 260);
            this.ExitButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.ExitButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Primary = false;
            this.ExitButton.Size = new System.Drawing.Size(284, 36);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.Text = "Exit";
            this.ExitButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ExitButton.TextUpperCase = false;
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.AutoSize = true;
            this.AboutButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AboutButton.Depth = 0;
            this.AboutButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.AboutButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.AboutButton.Icon = null;
            this.AboutButton.Location = new System.Drawing.Point(4, 205);
            this.AboutButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.AboutButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Primary = false;
            this.AboutButton.Size = new System.Drawing.Size(284, 36);
            this.AboutButton.TabIndex = 4;
            this.AboutButton.Text = "About";
            this.AboutButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AboutButton.TextUpperCase = false;
            this.AboutButton.UseVisualStyleBackColor = true;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // materialDivider1
            // 
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialDivider1.Location = new System.Drawing.Point(3, 147);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(286, 1);
            this.materialDivider1.TabIndex = 5;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // materialDivider2
            // 
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialDivider2.Location = new System.Drawing.Point(3, 250);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(286, 1);
            this.materialDivider2.TabIndex = 6;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // ThemeButton
            // 
            this.ThemeButton.AutoSize = true;
            this.ThemeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ThemeButton.Depth = 0;
            this.ThemeButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.ThemeButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.ThemeButton.Icon = null;
            this.ThemeButton.Location = new System.Drawing.Point(4, 157);
            this.ThemeButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.ThemeButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.ThemeButton.Name = "ThemeButton";
            this.ThemeButton.Primary = false;
            this.ThemeButton.Size = new System.Drawing.Size(284, 36);
            this.ThemeButton.TabIndex = 7;
            this.ThemeButton.Text = "Dark Theme";
            this.ThemeButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ThemeButton.TextUpperCase = false;
            this.ThemeButton.UseVisualStyleBackColor = true;
            this.ThemeButton.Click += new System.EventHandler(this.ThemeButton_Click);
            // 
            // HamburgerMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "HamburgerMenu";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Size = new System.Drawing.Size(298, 436);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.HamburgerMenu_Paint);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialFlatButton OpenButton;
        private MaterialSkin.Controls.MaterialFlatButton SaveButton;
        private MaterialSkin.Controls.MaterialFlatButton ShowCourseCodesButton;
        private MaterialSkin.Controls.MaterialFlatButton ExitButton;
        private MaterialSkin.Controls.MaterialFlatButton AboutButton;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private MaterialSkin.Controls.MaterialFlatButton ThemeButton;
    }
}
