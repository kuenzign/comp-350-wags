﻿#region copyright

////////////////////////////////////////////////
// FileName:          Constants.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/6/2017 at 7:36 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

namespace WaGS.Core
{
    internal class Constants
    {
        public const int LEVENSTEIN_CUTOFF = 6;
        public const int LEVENSTEIN_EXACT_MATCH_MODIFIER = 5;
        public const int LEVENSTEIN_FIRST_CHAR_MATCH_MODIFIER = 5;
        public const int PREREQUISITE_NOTIFICATION_DISPLAY_TIME = 1000;
        public const int PREREQUISITE_NOTIFICATION_ANIMATION_TIME = 300;
        public const string PREREQUISITE_FILE = ".\\Prerequisites.csv";
        public const string DATABASE_FILE_NAME = ".\\database.csv";
        public const string COMPLETED_FILE_NAME = "completed.csv";
        public const string FILE_DIALOG_FILTER = "Wags Schedule (*.wags)|*.wags|All files (*.*)|*.*";
    }
}