﻿#region copyright

////////////////////////////////////////////////
// FileName:          Section.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using System.Collections.Generic;

namespace WaGS.Core
{
    internal class Section
    {
        protected Course _parent;
        protected char _sectionLetter = '\0';
        protected int _enrollment = -1;
        protected int _capacity = -1;
        protected string _room = "";
        protected string _building = "";
        protected List<TimeSlot> _times;

        public Section()
        {
            _times = new List<TimeSlot>();
        }

        public Section(Course Parent)
        {
            _parent = Parent;
            _times = new List<TimeSlot>();
        }

        public char SectionLetter
        {
            get { return _sectionLetter; }
            set { _sectionLetter = value; }
        }

        public Course Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        public int Enrollment
        {
            get { return _enrollment; }
            set { _enrollment = value; }
        }

        public int Capacity
        {
            get { return _capacity; }
            set { _capacity = value; }
        }

        public List<TimeSlot> Times
        {
            get { return _times; }
            set { _times = value; }
        }

        public void addTime(TimeSlot time)
        {
            if (!_times.Contains(time))
            {
                _times.Add(time);
            }
        }

        public void removeTime(TimeSlot time)
        {
            _times.Remove(time);
        }

        /*
        public string getTwelveHourTime()
        {
            string[] startTimes = new string[2];
            startTimes[0] = "N/A";

            string[] endTimes = new string[2];
            endTimes[0] = "N/A";

            if (StartTime != "NULL")
            {
                startTimes = StartTime.Substring(0, StartTime.Length - 3).Split(':');
                int hours = Convert.ToInt32(startTimes[0]);
                if (hours > 12)
                {
                    hours -= 12;
                }
                startTimes[0] = hours.ToString() + ":" + startTimes[1];
            }

            if (StartTime != "NULL")
            {
                endTimes = EndTime.Substring(0, EndTime.Length - 3).Split(':');
                int hours = Convert.ToInt32(endTimes[0]);
                if (hours > 12)
                {
                    hours -= 12;
                }
                endTimes[0] = hours.ToString() + ":" + endTimes[1];
            }
            return startTimes[0] + "-" + endTimes[0];
        }
        */
    }
}