﻿#region copyright

////////////////////////////////////////////////
// FileName:          Location.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using System;

namespace WaGS.Core
{
    internal class Location
    {
        protected string _building = "";
        protected string _roomNumber = "";

        public Location()
        {
        }

        public String Building
        {
            get { return _building; }
            set { _building = value; }
        }

        public string RoomNumber
        {
            get { return _roomNumber; }
            set { _roomNumber = value; }
        }
    }
}