﻿#region copyright

////////////////////////////////////////////////
// FileName:          TimeSlot.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using System;

namespace WaGS.Core
{
    internal class TimeSlot
    {
        protected DateTime _startTime;
        protected DateTime _endTime;
        protected Location _locale;
        protected char _dayOfWeek;

        public TimeSlot()
        {
        }

        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        public Location Locale
        {
            get { return _locale; }
            set { _locale = value; }
        }

        public char dayOfWeek
        {
            get { return _dayOfWeek; }
            set
            {
                _dayOfWeek = value;
                setDayOfWeek(value);
            }
        }

        public void setDayOfWeek(char day)
        {
            _startTime = DateTime.Today;
            _endTime = DateTime.Today;
            //set StartTime and Endtime day of week to monday
            switch (DateTime.Today.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    _startTime = _startTime.AddDays(1);
                    _endTime = _endTime.AddDays(1);
                    break;

                case DayOfWeek.Monday:
                    break;

                case DayOfWeek.Tuesday:
                    _startTime = _startTime.AddDays(-1);
                    _endTime = _endTime.AddDays(-1);
                    break;

                case DayOfWeek.Wednesday:
                    _startTime = _startTime.AddDays(-2);
                    _endTime = _endTime.AddDays(-2);
                    break;

                case DayOfWeek.Thursday:
                    _startTime = _startTime.AddDays(-3);
                    _endTime = _endTime.AddDays(-3);
                    break;

                case DayOfWeek.Friday:
                    _startTime = _startTime.AddDays(-4);
                    _endTime = _endTime.AddDays(-4);
                    break;

                case DayOfWeek.Saturday:
                    _startTime = _startTime.AddDays(-5);
                    _endTime = _endTime.AddDays(-5);
                    break;
            }

            switch (day)
            {
                case 'M':
                    break;

                case 'T':
                    _startTime = _startTime.AddDays(1);
                    _endTime = _endTime.AddDays(1);
                    break;

                case 'W':
                    _startTime = _startTime.AddDays(2);
                    _endTime = _endTime.AddDays(2);
                    break;

                case 'R':
                    _startTime = _startTime.AddDays(3);
                    _endTime = _endTime.AddDays(3);
                    break;

                case 'F':
                    _startTime = _startTime.AddDays(4);
                    _endTime = _endTime.AddDays(4);
                    break;
            }
            _dayOfWeek = day;
        }

        public void setStartTime(string startTime)
        {
            _startTime = _startTime.Add(TimeSpan.Parse(startTime));
        }

        public void setEndTime(string endTime)
        {
            _endTime = _endTime.Add(TimeSpan.Parse(endTime));
        }
    }
}