﻿#region copyright

////////////////////////////////////////////////
// FileName:          CourseList.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WaGS.Core
{
    internal class CourseList : List<Course>
    {
        public CourseList()
        {
        }

        //Saves a csv file of the department and number
        public void save(string file)
        {
            StringBuilder sb = new StringBuilder();

            foreach (Course course in this)
            {
                sb.Append(course.Department);
                sb.Append(",");
                sb.Append(course.CourseNumber);
                //sb.Append(",");
                //sb.Append(course.Section.?);
                //sb.Append("A"); //Will be corrected once course/section classes have been updated
                sb.Append("\n");
            }
            System.IO.File.WriteAllText(file, sb.ToString());
        }

        public void open(string file, Database db)
        {
            List<Course> newCourses = new List<Course>();

            if (File.Exists(file))
            {
                using (CsvFileReader reader = new CsvFileReader(file))
                {
                    CsvRow row = new CsvRow();
                    while (reader.ReadRow(row))
                    {
                        string department = row.ElementAt(0);
                        int courseNumber = Int32.Parse(row.ElementAt(1));
                        newCourses.Add(db.getCourse(department, courseNumber));
                    }
                }
            }

            foreach (Course c in newCourses)
            {
                if (!Contains(c) && !(c == null))
                {
                    Add(c);
                }
            }
        }
    }
}