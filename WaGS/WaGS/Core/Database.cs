﻿#region copyright

////////////////////////////////////////////////
// FileName:          Database.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace WaGS.Core
{
    internal class Database
    {
        // Use example for this is courses["<department>"][<course level> - 1][<course number>]
        // use (courseNo / 100) - 1 for courseindex
        private Dictionary<string, Dictionary<int, Course>[]> courses;

        private Dictionary<string, Dictionary<int, Course>[]> prerequisites;
        private CourseList completedCourses;

        // initializes the local variables for the database
        public Database()
        {
            courses = new Dictionary<string, Dictionary<int, Course>[]>();
            prerequisites = new Dictionary<string, Dictionary<int, Course>[]>();
            completedCourses = new CourseList();
        }

        public CourseList getAllCourses()
        {
            CourseList ret = new CourseList();
            foreach (KeyValuePair<string, Dictionary<int, Course>[]> dp in courses)
            {
                for (int i = 0; i < 4; i++)
                {
                    foreach (KeyValuePair<int, Course> cs in dp.Value[i])
                        ret.Add(cs.Value);
                }
            }
            foreach (KeyValuePair<string, Dictionary<int, Course>[]> dp in prerequisites)
            {
                for (int i = 0; i < 4; i++)
                {
                    foreach (KeyValuePair<int, Course> cs in dp.Value[i])
                        ret.Add(cs.Value);
                }
            }
            //ret.Sort();
            return ret;
        }

        // Function to get a list of all departments that exist in the database
        public string[] getDepartments()
        {
            string[] ret = courses.Keys.ToArray();
            Array.Sort(ret);
            return ret;
        }

        // Returns a list of sections from within a class that match the query
        private Course getSections(Course course, query request)
        {
            Course ret = new Course(course);
            ret.Sections.Clear();
            DateTime startTime;
            DateTime endTime;
            CultureInfo culture = CultureInfo.CurrentCulture;

            if (!DateTime.TryParseExact(request.startTime, new[] { "hh:mm tt", "h:mm tt" }, culture, DateTimeStyles.None, out startTime))
                startTime = DateTime.MinValue;

            if (!DateTime.TryParseExact(request.endTime, new[] { "hh:mm tt", "h:mm tt" }, culture, DateTimeStyles.None, out endTime))
                endTime = DateTime.MaxValue;

            foreach (Section sec in course.Sections)
            {
                bool allOK = true;
                bool ran = false;
                foreach (TimeSlot ts in sec.Times)
                {
                    ran = true;
                    if (!(ts.StartTime.Hour >= startTime.Hour && ts.EndTime.Hour < endTime.Hour))
                    {
                        allOK = false;
                    }
                }
                if (allOK && ran)
                {
                    ret.Sections.Add(sec);
                }
            }
            return ret;
        }

        // Returns a list of courses according to the request
        private List<Course> getCourses(Dictionary<int, Course> Course, query request)
        {
            List<Course> ret = new List<Course>();
            foreach (KeyValuePair<int, Course> Class in Course)
            {
                if (request.startTime == "-" && request.endTime == "-")
                    ret.Add(Class.Value);
                else
                    ret.Add(getSections(Class.Value, request));
            }
            return ret;
        }

        // Returns a list of courses within a level according to the request
        private List<Course> getCourseLevel(Dictionary<int, Course>[] dep, query request)
        {
            List<Course> ret = new List<Course>();
            if (request.courselevel == -1)
            {
                for (int i = 0; i < 4; i++)
                {
                    ret.AddRange(getCourses(dep[i], request));
                }
            }
            else
            {
                ret.AddRange(getCourses(dep[(request.courselevel / 100) - 1], request));
            }
            return ret;
        }

        public CourseList queryPrerequisites(query request)
        {
            CourseList ret = query(request);
            if (prerequisites.ContainsKey(request.department))
            {
                ret.AddRange(getCourseLevel(prerequisites[request.department], request));
            }
            else if (request.department == "-")
            {
                foreach (KeyValuePair<string, Dictionary<int, Course>[]> dep in prerequisites)
                {
                    ret.AddRange(getCourseLevel(dep.Value, request));
                }
            }
            return ret;
        }

        // takes a query object from the search class and returns the classes that are valid for that query
        public CourseList query(query request)
        {
            CourseList ret = new CourseList();
            if (courses.ContainsKey(request.department))
            {
                ret.AddRange(getCourseLevel(courses[request.department], request));
            }
            else
            {
                foreach (KeyValuePair<string, Dictionary<int, Course>[]> dep in courses)
                {
                    ret.AddRange(getCourseLevel(dep.Value, request));
                }
            }
            return ret;
        }

        private Course getCourseFrom(Dictionary<string, Dictionary<int, Course>[]> source, string department, int number)
        {
            if (source.ContainsKey(department) && number > 100 && number < 500 && source[department][(number / 100) - 1].ContainsKey(number))
                return source[department][(number / 100) - 1][number];
            else
                return null;
        }

        // function to get any given course returns null if the course doesn't exist
        // NEEDS A TESTER
        public Course getCourse(string department, int number)
        {
            return getCourseFrom(courses, department, number);
        }

        public Course getPrerequisite(string department, int number)
        {
            return getCourseFrom(prerequisites, department, number);
        }

        // takes in database row and converts it to a section
        private Section rowToSection(CsvRow row)
        {
            Section ret = new Section();
            string sectionCode = row[0].Substring(row[0].Length - 1, 1);

            //row[5] is the string with all possible days
            //add a timeslot for every day that the section is held
            foreach (char day in row[5])
            {
                TimeSlot ts = new TimeSlot();
                ts.dayOfWeek = day;
                ts.setStartTime(row[3]);//row[3] is the start time string
                ts.setEndTime(row[4]);//row[4] is the end time string

                Location l = new Location();
                l.Building = row[6];
                l.RoomNumber = row[7];
                ts.Locale = l;

                ret.addTime(ts);
            }

            ret.Enrollment = Int32.Parse(row[8]);
            ret.Capacity = Int32.Parse(row[9]);
            ret.SectionLetter = Convert.ToChar(sectionCode);
            return ret;
        }

        // generates the course if it doesn't exist and fills the department and courseNo
        private bool generatedCourse(Dictionary<string, Dictionary<int, Course>[]> source, string department, int courseNo)
        {
            int courseLevel = (courseNo / 100) - 1;

            if (!source.ContainsKey(department))
            {
                source.Add(department, new Dictionary<int, Course>[4]);
                for (int i = 0; i < 4; i += 1)
                {
                    source[department][i] = new Dictionary<int, Course>();
                }
            }
            if (!source[department][courseLevel].ContainsKey(courseNo))
            {
                source[department][courseLevel].Add(courseNo, new Course());
                source[department][courseLevel][courseNo].CourseNumber = courseNo;
                source[department][courseLevel][courseNo].Department = department;
                return true;
            }
            return false;
        }

        // Load database file into the program
        public void load()
        {
            if (File.Exists(Constants.DATABASE_FILE_NAME))
            {
                using (CsvFileReader reader = new CsvFileReader(Constants.DATABASE_FILE_NAME))
                {
                    CsvRow row = new CsvRow();
                    while (reader.ReadRow(row))
                    {
                        string courseString = row[0];
                        string department = courseString.Substring(0, 4);

                        int courseNo = Int32.Parse(courseString.Substring(5, 3));
                        int courseLevel = (courseNo / 100) - 1;

                        Section currentSection = rowToSection(row);

                        if (generatedCourse(courses, department, courseNo))
                        {
                            courses[department][courseLevel][courseNo].ShortTitle = row[1];
                            courses[department][courseLevel][courseNo].LongTitle = row[2];
                        }

                        currentSection.Parent = courses[department][courseLevel][courseNo];
                        Section existingSection = courses[department][courseLevel][courseNo].Sections.Find(x => x.SectionLetter == currentSection.SectionLetter);
                        if (existingSection != null)
                        {
                            existingSection.Times.AddRange(currentSection.Times);
                        }
                        else
                        {
                            courses[department][courseLevel][courseNo].Sections.Add(currentSection);
                        }
                    }
                }
            }
            else
            {
                Console.Write("Could not open database file");
            }
            loadPrerequisites();
            completedCourses.open("completed.csv", this);
        }

        public void loadPrerequisites()
        {
            if (File.Exists(Constants.PREREQUISITE_FILE))
            {
                using (CsvFileReader reader = new CsvFileReader(Constants.PREREQUISITE_FILE))
                {
                    CsvRow row = new CsvRow();
                    while (reader.ReadRow(row))
                    {
                        string courseString = row[0];
                        string department = courseString.Substring(0, 4);
                        int courseNo = Int32.Parse(courseString.Substring(5, 3));

                        Course current = getCourse(department, courseNo);

                        if (current == null)
                        {
                            generatedCourse(prerequisites, department, courseNo);

                            current = getCourseFrom(prerequisites, department, courseNo);
                            if (current == null)
                                continue;
                        }

                        string prereqString = row[1];
                        string prereqDepartment = prereqString.Substring(0, 4);

                        int prereqNo = Int32.Parse(prereqString.Substring(5, 3));

                        Course prereq = getCourseFrom(courses, prereqDepartment, prereqNo);

                        if (prereq == null)
                        {
                            generatedCourse(prerequisites, prereqDepartment, prereqNo);
                            prereq = getCourseFrom(prerequisites, prereqDepartment, prereqNo);
                        }
                        if (prereq != null)
                        {
                            current.PrerequisiteList.Add(prereq);
                        }
                    }
                }
            }
            else
            {
            }
        }

        // Stub function currently "in development" should not be reviewed or considered
        public void export()
        {
            using (System.IO.BinaryWriter dbFile = new System.IO.BinaryWriter(new FileStream(".\\Database.wagsdb", FileMode.Open, FileAccess.Read)))
            {
                foreach (KeyValuePair<string, Dictionary<int, Course>[]> department in courses)
                {
                    foreach (Dictionary<int, Course> courseLevel in department.Value)
                    {
                        foreach (KeyValuePair<int, Course> course in courseLevel)
                        {
                            foreach (Section section in course.Value.Sections)
                            {
                            }
                        }
                    }
                }
            }
        }
    }
}