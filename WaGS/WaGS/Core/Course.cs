﻿#region copyright

////////////////////////////////////////////////
// FileName:          Course.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using System.Collections.Generic;

namespace WaGS.Core
{
    internal class Course
    {
        protected string _department;
        protected int _courseNumber;
        protected List<Section> _section = new List<Section>();
        protected CourseList _prerequisiteList = new CourseList();
        protected string _shortTitle;
        protected string _longTitle;

        public Course(Course toCopy)
        {
            _department = toCopy.Department;
            _courseNumber = toCopy.CourseNumber;
            _shortTitle = toCopy.ShortTitle;
            _longTitle = toCopy.LongTitle;
            _section.AddRange(toCopy.Sections);
        }

        public string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        public int CourseNumber
        {
            get { return _courseNumber; }
            set { _courseNumber = value; }
        }

        public List<Section> Sections
        {
            get { return _section; }
            set { _section = value; }
        }

        public CourseList PrerequisiteList
        {
            get { return _prerequisiteList; }
            set { _prerequisiteList = value; }
        }

        public string ShortTitle
        {
            get { return _shortTitle; }
            set { _shortTitle = value; }
        }

        public string LongTitle
        {
            get { return _longTitle; }
            set { _longTitle = value; }
        }

        public void addSection(char c)
        {
            Section s = new Section();
            s.SectionLetter = c;
            s.Parent = this;
            _section.Add(s);
        }

        public Course()
        {
            _section = new List<Section>();
        }
    }
}