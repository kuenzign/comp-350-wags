﻿#region copyright

////////////////////////////////////////////////
// FileName:          SectionList.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WaGS.Core
{
    internal class SectionList : List<Section>
    {
        public SectionList()
        {
        }

        //Saves a csv file of the Section Letter
        public void save(string file)
        {
            StringBuilder sb = new StringBuilder();

            foreach (Section section in this)
            {
                sb.Append(section.Parent.Department);
                sb.Append(",");
                sb.Append(section.Parent.CourseNumber);
                sb.Append(",");
                sb.Append(section.SectionLetter);
                sb.Append("\n");
            }

            System.IO.File.WriteAllText(file, sb.ToString());
        }

        public void open(string file, Database db)
        {
            List<Section> newSections = new List<Section>();

            if (File.Exists(file))
            {
                using (CsvFileReader reader = new CsvFileReader(file))
                {
                    CsvRow row = new CsvRow();
                    while (reader.ReadRow(row))
                    {
                        Course searchResult = new Course();
                        string department = row.ElementAt(0);
                        int courseNumber = Int32.Parse(row.ElementAt(1));
                        char sectionLetter = Convert.ToChar(row.ElementAt(2));
                        searchResult = db.getCourse(department, courseNumber);
                        if (searchResult != null)
                        {
                            foreach (Section s in searchResult.Sections)
                            {
                                if (s.SectionLetter == sectionLetter)
                                {
                                    newSections.Add(s);
                                }
                            }
                        }
                    }
                }
            }

            foreach (Section s in newSections)
            {
                if (!Contains(s) && !(s == null))
                {
                    Add(s);
                }
            }
            newSections.Clear();
        }
    }
}