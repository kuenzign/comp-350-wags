﻿#region copyright

////////////////////////////////////////////////
// FileName:          Search.cs
// Namespace:         WaGS.Core
// Project:           WaGS
//
// Created On:        5/2/2017 at 1:51 PM
// Last Modified On:  5/7/2017 at 8:33 PM
////////////////////////////////////////////////

#endregion copyright

using System;

namespace WaGS.Core
{
    internal class Search
    {
        private CourseList _searchResults;
        private string _dept;
        private string _courseLevel;
        private string _startTime;
        private string _endTime;
        private string _searchBarText;

        public CourseList SearchResults
        {
            get { return _searchResults; }
            set { _searchResults = value; }
        }

        public string Dept
        {
            get { return _dept; }
            set { _dept = value; }
        }

        public string CourseLevel
        {
            get { return _courseLevel; }
            set { _courseLevel = value; }
        }

        public string StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        public string EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        public string SearchBarText
        {
            get { return _searchBarText; }
            set { _searchBarText = value; }
        }

        //The constructor takes each of the search parameters in, and stores them locally as strings
        public Search(string dept, string courseLevel, string startTime, string endTime, string searchBarText)
        {
            Dept = dept;
            CourseLevel = courseLevel;
            StartTime = startTime;
            EndTime = endTime;
            SearchBarText = searchBarText;
        }

        //Returns a query, using the values from the UI elements that are stored in search after the search button is clicked
        public query generateQuery()
        {
            query q;
            q.department = Dept;
            q.startTime = StartTime;
            q.endTime = EndTime;
            if (CourseLevel.Length < 3 || !Int32.TryParse(CourseLevel.Substring(0, 3), out q.courselevel))
                q.courselevel = -1;

            return q;
        }

        //Some code taken from http://rosettacode.org/wiki/Levenshtein_distance#C.2B.2B
        public int LevenshteinDistance(string s, string t)
        {
            //make search case insensitive
            s = s.ToUpper();
            t = t.ToUpper();

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            for (int i = 0; i <= n; i++)
            {
                d[i, 0] = i;
            }
            for (int j = 0; j <= m; j++)
            {
                d[0, j] = j;
            }

            for (int j = 1; j <= m; j++)
            {
                for (int i = 1; i <= n; i++)
                {
                    if (s[i - 1] == t[j - 1])
                    {
                        d[i, j] = d[i - 1, j - 1];  //no operation
                    }
                    else
                    {
                        d[i, j] = Math.Min(Math.Min(
                            d[i - 1, j] + 1,    //a deletion
                            d[i, j - 1] + 1),   //an insertion
                            d[i - 1, j - 1] + 1 //a substitution
                            );
                    }
                }
            }
            return d[n, m];
        }
    }

    //Defines what a "query" is...for now...
    internal struct query
    {
        public string department;
        public int courselevel;
        public string startTime;
        public string endTime;
    }
}