﻿#region copyright

////////////////////////////////////////////////
// FileName:          DatabaseTests.cs
// Namespace:         UnitTesting
// Project:           UnitTesting
//
// Created On:        5/6/2017 at 7:36 PM
// Last Modified On:  5/7/2017 at 8:34 PM
////////////////////////////////////////////////

#endregion copyright

using Microsoft.VisualStudio.TestTools.UnitTesting;
using WaGS.Core;

namespace UnitTesting
{
    [TestClass]
    public class DatabaseTests
    {
        /* [TestMethod]
         public void PrereqLoadTest()
         {
             Database db = new Database();
             db.load();

             Course testCourse = new Course();
             testCourse = db.getPrerequisite("ACCT", 201);

             Assert.AreEqual(testCourse.Department, "ACCT");

             Assert.AreEqual(testCourse.CourseNumber, 201);
         }*/

        [TestMethod]
        public void PrerequLoadTest()
        {
            Database db = new Database();
            db.load();

            Course testCourse = new Course();

            testCourse = db.getPrerequisite("ACCT", 201);

            Assert.AreEqual("ACCT", testCourse.Department);
            Assert.AreEqual(201, testCourse.CourseNumber);
        }

        [TestMethod]
        public void CoursesHavePrerequisitesTest()
        {
            Database db = new Database();
            db.load();

            Course testCourse = new Course();

            Course prereq = db.getPrerequisite("ACCT", 201);

            testCourse = db.getCourse("ACCT", 202);

            Assert.AreEqual(true, testCourse.PrerequisiteList.Contains(prereq));
        }
    }
}