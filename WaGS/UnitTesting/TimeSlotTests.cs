﻿#region copyright

////////////////////////////////////////////////
// FileName:          TimeSlotTests.cs
// Namespace:         UnitTesting
// Project:           UnitTesting
//
// Created On:        5/6/2017 at 7:36 PM
// Last Modified On:  5/7/2017 at 8:34 PM
////////////////////////////////////////////////

#endregion copyright

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WaGS.Core;

namespace UnitTesting
{
    [TestClass]
    public class TimeSlotTests
    {
        //just a sample test for example purposes
        [TestMethod]
        public void DateTimeTest()
        {
            //arrange
            DateTime dt1;
            DateTime dt2;

            dt1 = DateTime.Now;
            //act

            dt2 = dt1;
            //assert
            Assert.AreEqual(dt1, dt2);
        }

        [TestMethod]
        public void TimeSlotTest()
        {
            TimeSlot ts = new TimeSlot();
            ts.setDayOfWeek('M');
            ts.setStartTime("8:00:00");
            ts.setEndTime("8:50:00");

            Console.WriteLine();
            Assert.AreEqual(ts.StartTime.ToShortTimeString(), "8:00 AM");
            Assert.AreEqual(ts.EndTime.ToShortTimeString(), "8:50 AM");
            Assert.AreEqual(ts.dayOfWeek, 'M');
        }
    }
}